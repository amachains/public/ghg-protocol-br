#!/usr/bin/awk -f
BEGIN { FS="\t";print "{\t" }
{
    printf "\"%s\" : {\n",$1
    printf "\t\"unit\" : \"%s\",\n",$2
    printf "\t\"co2Factor\" : %s,\n",$6
    printf "\t\"ch4Factor\" : %s,\n",$7
    printf "\t\"n2oFactor\" : %s\n",$8
    printf "\t},\n"
}
END {
}