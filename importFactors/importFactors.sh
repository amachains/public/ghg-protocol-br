#!/bin/bash

set +x

#
# Stationary Fossil Fuel
#

cat input/stationaryFossilFuelFactors.txt |
# remove pontos do numeros
sed 's/\([0-9]\)\.\([0-9]\)/\1\2/g' |
# troca as virgulas por ponto
sed 's/,/./g' |
# Limpeza simples, todos os espaços com tabulações viram tabulações
sed 's/\t\t-/\t0/g' |
sed 's/\t-/\t0/g' |
sed 's/\t\t/\t/g' |
sed 's/^ //g'|  
# ajustar unidades
sed 's/m³/m3/g'  |
./stationaryFuelFactorsToJSON.sh  > output/stationaryFossilFuelFactors.json
sed -i '$ s/,$/\n}/' output/stationaryFossilFuelFactors.json
#TODO: Ainda é preciso colocar um 0 no final do primeiro elemento, aparentemente só acontece a linha com -


#
# Stationary Bio Fuel
#

cat input/stationaryBioFuelFactors.txt |

# remove pontos do numeros
sed 's/\([0-9]\)\.\([0-9]\)/\1\2/g' |

# troca as virgulas por ponto
sed 's/,/./g' |
# Limpeza simples, todos os espaços com tabulações viram tabulações
sed 's/\t\t-/\t0/g' |
sed 's/\t-/\t0/g' |
sed 's/\t\t/\t/g' |
sed 's/^ //g'|  
# ajustar unidades
sed 's/m³/m3/g'  |
./stationaryFuelFactorsToJSON.sh  > output/stationaryBioFuelFactors.json
sed -i '$ s/,$/\n}/' output/stationaryBioFuelFactors.json

#
# Mobile Fossil Fuel
#

cat input/mobileRoadFossilFuelFactors.txt |

# remove pontos do numeros
sed 's/\([0-9]\)\.\([0-9]\)/\1\2/g' |

# troca as virgulas por ponto
sed 's/,/./g' |
# Limpeza simples, todos os espaços com tabulações viram tabulações
sed 's/\t\t-/\t0/g' |
sed 's/\t-/\t0/g' |
sed 's/\t\t/\t/g' |
sed 's/^ //g'|  
# ajustar unidades
sed 's/m³/m3/g'  |
./mobileFuelFactorsToJSON.sh  > output/mobileRoadFossilFuelFactors.json
sed -i '$ s/,$/\n}/' output/mobileFossilFuelFactors.json

#
# Mobile Bio Fuel
#

cat input/mobileRoadBioFuelFactors.txt |

# remove pontos do numeros
sed 's/\([0-9]\)\.\([0-9]\)/\1\2/g' |

# troca as virgulas por ponto
sed 's/,/./g' |
# Limpeza simples, todos os espaços com tabulações viram tabulações
sed 's/\t\t-/\t0/g' |
sed 's/\t-/\t0/g' |
sed 's/\t\t/\t/g' |
sed 's/^ //g'|  
# ajustar unidades
sed 's/m³/m3/g'  |
./mobileFuelFactorsToJSON.sh  > output/mobileRoadBioFuelFactors.json
sed -i '$ s/,$/\n}/' output/mobileRoadBioFuelFactors.json

#
# Mobile Coal Rail Transport Fuel Factors
#

cat input/mobileCoalRailFuelFactors.txt |

# remove pontos do numeros
sed 's/\([0-9]\)\.\([0-9]\)/\1\2/g' |

# troca as virgulas por ponto
sed 's/,/./g' |
# Limpeza simples, todos os espaços com tabulações viram tabulações
sed 's/\t\t-/\t0/g' |
sed 's/\t-/\t0/g' |
sed 's/\t\t/\t/g' |
sed 's/^ //g'|  
# ajustar unidades
sed 's/m³/m3/g'  |
./mobileFuelFactorsToJSON.sh  > output/mobileCoalRailFuelFactors.json
sed -i '$ s/,$/\n}/' output/mobileCoalRailFuelFactors.json


#
# Global Warming Potential Gas Factors
#
cat input/globalWarmingPotentialGasFactors.txt |

# remove pontos do numeros
sed 's/\([0-9]\)\.\([0-9]\)/\1\2/g' |

# troca as virgulas por ponto
sed 's/,/./g' |

./globalWarmingPotentialGasFactorsToJSON.sh  > output/globalWarmingPotentialGasFactors.json

# remove a ultima virgula
sed -i  '$ s/\(.*\),/\1\n}/' output/globalWarmingPotentialGasFactors.json
