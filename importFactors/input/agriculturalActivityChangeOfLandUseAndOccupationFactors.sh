#!/bin/bash

set +x
#
# Stationary Fossil Fuel
#
cat `basename $0 .sh`.txt |

sed '1d' | 
# remove pontos do numeros
sed 's/\([0-9]\)\.\([0-9]\)/\1\2/g' |
# troca as virgulas do numeros por ponto
sed 's/\([0-9]\)\,\([0-9]\)/\1.\2/g' |
# deletar a primeira linha com o cabeçalho

awk '
BEGIN { FS="\t";print "{\t" }
{
    if ($3 == "-")
        printf "\t\"%s\" : {\n",$1
    else
        printf "\t\"%s - %s\" : {\n",$1,$3
    printf "\t\t\"%s\" : {\n",$2
    printf "\t\t\t\"unit\" : \"%s\",\n",$5
    printf "\t\t\t\"co2EqFactor\" : %s\n",$4
    printf "\t\t}\n"
    printf "\t},\n"
}
END {
}' > ../output/`basename $0 .sh`.json

# sed -i '$ s/,$/\n}/' ../output/`basename $0 .sh`.json