#!/bin/bash

set +x

#
# Stationary Fossil Fuel
#
cat `basename $0 .sh`.txt |

# remove pontos do numeros
sed 's/\([0-9]\)\.\([0-9]\)/\1\2/g' |
# troca as virgulas do numeros por ponto
sed 's/\([0-9]\)\,\([0-9]\)/\1.\2/g' |
# Limpeza simples, todos os espaços com tabulações viram tabulações
#./stationaryFuelFactorsToJSON.sh  > output/stationaryFossilFuelFactors.json
#sed -i '$ s/,$/\n}/' output/stationaryFossilFuelFactors.json

awk '
BEGIN { FS="\t";print "{\t" }
{
    if ($3)
        printf "\t\"%s - %s %s\" : {\n",$1,$2,$3
    else
        printf "\t\"%s - %s\" : {\n",$1,$2
    printf "\t\t\"unit\" : \"%s\",\n",$5
    printf "\t\t\"co2Factor\" : %s\n",$4
    printf "\t},\n"
}
END {
}' > `basename $0 .sh`.json

sed -i '$ s/,$/\n}/' `basename $0 .sh`.json