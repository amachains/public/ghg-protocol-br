#!/usr/bin/awk -f
BEGIN { FS="\t";print "{\t" }
{
    printf "\"%s\" : {\n",$1
    printf "\t\"unit\" : \"%s\",\n",$3
    printf "\t\"co2Factor\" : %s,\n",$17
    printf "\t\"ch4Factor\" : {\n"
    printf "\t\t\"Energia\" : %s ,\n",$18
    printf "\t\t\"Manufatura ou Construção\" : %s ,\n",$19
    printf "\t\t\"Comercial ou Institucional\" : %s ,\n",$20
    printf "\t\t\"Residencial, Agricultura, Florestal ou Pesca\" : %s \n",$21
    printf "\t\},\n"
    printf "\t\"n2oFactor\" : {\n"
    printf "\t\t\"Energia\" : %s ,\n",$22
    printf "\t\t\"Manufatura ou Construção\" : %s ,\n",$23
    printf "\t\t\"Comercial ou Institucional\" : %s ,\n",$24
    printf "\t\t\"Residencial, Agricultura, Florestal ou Pesca\" : %s \n",$25
    printf "\t\}\n"
    printf "\t},\n"
}
END {
}