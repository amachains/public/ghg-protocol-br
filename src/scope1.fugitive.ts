/* eslint-disable @typescript-eslint/no-explicit-any */
import { GhgProtocol } from './ghgProtocol';

export function setFugitiveGasConsumption(
  this: GhgProtocol,
  description: string,
  { gasName, stockVariation, transferedQuantity, capacityVariation }: any,
) {
  if (this.emissionFactors.globalWarmingPotentialGasFactors.has(gasName)) {
    this.consumptions.fugitiveGasConsumptions.set(description, {
      gasName,
      stockVariation,
      transferedQuantity,
      capacityVariation,
    });
    return true;
  } else {
    return false;
  }
}

export function calcFugitiveGasEmissions(this: GhgProtocol) {
  let tonCo2Eq = 0;
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  this.consumptions.fugitiveGasConsumptions.forEach((values, _key) => {
    tonCo2Eq +=
      ((values.stockVariation +
        values.transferedQuantity -
        values.capacityVariation) *
        this.emissionFactors.globalWarmingPotentialGasFactors.get(
          values.gasName,
        )) /
      1000;
  });
  return {
    tonCo2Eq: tonCo2Eq,
  };
}

export function setFugitiveSF6andNF3Consumption(
  this: GhgProtocol,
  description: string,
  { gasName, stockInitialQuantity, stockFinalQuantity, purchasedQuantity }: any,
) {
  if (
    [
      'Trifluoreto de nitrogênio (NF3)',
      'Hexafluoreto de enxofre (SF6)',
    ].includes(gasName)
  ) {
    this.consumptions.fugitiveSF6andNF3Consumptions.set(description, {
      gasName,
      stockInitialQuantity,
      stockFinalQuantity,
      purchasedQuantity,
    });
    return true;
  } else {
    return false;
  }
}

export function calcSf6andNf3FugitiveGasEmissions(this: GhgProtocol) {
  let tonCo2Eq = 0;
  let kgSf6 = 0;
  let kgNf3 = 0;
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  this.consumptions.fugitiveSF6andNF3Consumptions.forEach((values, _key) => {
    if (values.gasName === 'Hexafluoreto de enxofre (SF6)') {
      kgSf6 +=
        values.stockInitialQuantity -
        values.stockFinalQuantity +
        values.purchasedQuantity;
    } else if (values.gasName === 'Trifluoreto de nitrogênio (NF3)') {
      kgNf3 +=
        values.stockInitialQuantity -
        values.stockFinalQuantity +
        values.purchasedQuantity;
    }
  });
  tonCo2Eq +=
    (kgSf6 *
      this.emissionFactors.globalWarmingPotentialGasFactors.get(
        'Hexafluoreto de enxofre (SF6)',
      )) /
    1000;
  tonCo2Eq +=
    (kgNf3 *
      this.emissionFactors.globalWarmingPotentialGasFactors.get(
        'Trifluoreto de nitrogênio (NF3)',
      )) /
    1000;
  return {
    kgSf6: kgSf6,
    kgNf3: kgNf3,
    tonCo2Eq: tonCo2Eq,
  };
}

export function setFugitiveOtherSourceToolsGasConsumption(
  this: GhgProtocol,
  description: string,
  { gasName, quantity }: any,
) {
  if (this.emissionFactors.globalWarmingPotentialGasFactors.has(gasName)) {
    this.consumptions.fugitiveOtherSourceToolsGasConsumptions.set(description, {
      gasName,
      quantity,
    });
    return true;
  } else {
    return false;
  }
}

export function calcFugitiveOtherSourceToolsGasEmissions(this: GhgProtocol) {
  let tonCo2Eq = 0;
  this.consumptions.fugitiveOtherSourceToolsGasConsumptions.forEach(
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    (values, _key) => {
      tonCo2Eq +=
        (values.quantity *
          this.emissionFactors.globalWarmingPotentialGasFactors.get(
            values.gasName,
          )) /
        1000;
    },
  );
  return {
    tonCo2Eq: tonCo2Eq,
  };
}
