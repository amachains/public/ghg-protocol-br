/* eslint-disable @typescript-eslint/no-explicit-any */
'use strict';

// import the factors
import agriculturalActivityChangeOfLandUseAndOccupationFactorsJSON from './emissionFactors/agriculturalActivityChangeOfLandUseAndOccupationFactors.json' ;
import agriculturalActivityCropResiduesBurningFactorsJSON from './emissionFactors/agriculturalActivityCropResiduesBurningFactors.json';
import agriculturalActivityCropResiduesDecompositionFactorsJSON from './emissionFactors/agriculturalActivityCropResiduesDecompositionFactors.json';
import agriculturalActivityFertilizingFactorsJSON from './emissionFactors/agriculturalActivityFertilizingFactors.json';
import agriculturalActivityGreenFertilizerFactorsJSON from './emissionFactors/agriculturalActivityGreenFertilizerFactors.json';
import agriculturalActivityIndirectEmissionsFactorsJSON from './emissionFactors/agriculturalActivityIndirectEmissionsFactors.json';
import agriculturalActivityOrganicSoilCultivationFactorsJSON from './emissionFactors/agriculturalActivityOrganicSoilCultivationFactors.json';
import agriculturalActivitySecondaryEmissionsFactorsJSON from './emissionFactors/agriculturalActivitySecondaryEmissionsFactors.json';
import carbonStockNativeVegetationFactorsJSON from './emissionFactors/carbonStockNativeVegetationFactors.json';
import finFactorsByYearJSON from './emissionFactors/finFactorsByYear.json';
import globalWarmingPotentialGasFactorsJSON from './emissionFactors/globalWarmingPotentialGasFactors.json';
import mobileCoalRailFuelFactorsJSON from './emissionFactors/mobileCoalRailFuelFactors.json';
import mobileRoadBioFuelFactorsJSON from './emissionFactors/mobileRoadBioFuelFactors.json';
import mobileRoadFossilFuelFactorsJSON from './emissionFactors/mobileRoadFossilFuelFactors.json';
import percentageMixedFuelByYearJSON from './emissionFactors/percentageMixedFuelByYear.json';
import stationaryBioFuelFactorsJSON from './emissionFactors/stationaryBioFuelFactors.json';
import stationaryFossilFuelFactorsJSON from './emissionFactors/stationaryFossilFuelFactors.json';
import {
  calcAgriculturalActivityAtmosphericDepositionEmission,
  calcAgriculturalActivityCropResiduesBurningEmissions,
  calcAgriculturalActivityCropResiduesDecompositionEmissions,
  calcAgriculturalActivityManagementCultureLandUseEmissions,
  calcAgriculturalActivityManagementCultureOrganicSoilEmissions,
  setAgriculturalActivityManagementCulture,
  setAgriculturalActivityManagementCultureLandUse,
} from './scope1.agriculturalActivity';
import {
  calcAgriculturalActivityLimingEmissions,
  setAgriculturalActivityLimingConsumption,
} from './scope1.agriculturalActivity';
import {
  calcAgriculturalActivityUreaEmissions,
  setAgriculturalActivityUreaConsumption,
} from './scope1.agriculturalActivity';
import {
  calcAgriculturalActivitySyntheticFertilizerEmissions,
  setAgriculturalActivitySyntheticFertilizerConsumption,
} from './scope1.agriculturalActivity';
import {
  calcAgriculturalActivityOrganicFertilizerEmissions,
  setAgriculturalActivityOrganicFertilizerConsumption,
} from './scope1.agriculturalActivity';
import {
  calcAgriculturalActivityGreenFertilizerEmissions,
  setAgriculturalActivityGreenFertilizerConsumption,
} from './scope1.agriculturalActivity';
import {
  calcFugitiveGasEmissions,
  calcFugitiveOtherSourceToolsGasEmissions,
  calcSf6andNf3FugitiveGasEmissions,
  setFugitiveGasConsumption,
  setFugitiveOtherSourceToolsGasConsumption,
  setFugitiveSF6andNF3Consumption,
} from './scope1.fugitive';
import {
  calcMobileAirEmissions,
  calcMobileHydroEmissions,
  calcMobileRailEmissions,
  calcMobileRoadEmissions,
  partialCalcMobileMixedFuelEmissions,
  setMobileAirFuelConsumption,
  setMobileHydroFuelConsumption,
  setMobileRailFuelConsumption,
  setMobileRoadFuelConsumption,
} from './scope1.mobile';
import {
  calcStationaryEmissions,
  setStationaryFuelConsumption,
} from './scope1.stationary';
import { calcPowerEmissions, setPowerConsumption } from './scope2';
import {
  calcCat1Emissions,
  calcCat2Emissions,
  setCat1Consumption,
  setCat2Consumption,
} from './scope3';

// pt-br Calagem
// en-us Liming
export const AGRICULTURAL_LIMINGSUBSTANCES = [
  'Calcário Calcítico',
  'Calcário Dolomítico',
];
export const AGRICULTURAL_ORGANIC_FERTILIZER = [
  'Composto orgânico',
  'Adubo orgânico geral',
  'Torta de filtro',
  'Vinhaça',
  'Esterco Bovino, equino, suino, ovino',
  'Esterco Avícola',
];
// const AGRICULTURAL_VEGETABLE_RESIDUE_MANAGEMENT = ["Decomposição", "Queima"];
export const AGRICULTURAL_CULTURES = [
  'Algodão',
  'Feijão',
  'Milho',
  'Soja',
  'Trigo',
];
export const CROPS_WITH_SECONDARY_HARVEST = [
  'Algodão',
  'Feijão',
  'Milho',
  'Soja',
  'Trigo',
  'Arroz',
  'Cana',
  'Pastagem',
];
// const CROPS_WITH_ICLF = ["Eucalipto", "Pinus", "Parica", "Seringueira", "Teca"];

// IPCC, 2013
// const c_co2Factor = 3.66;
// const co_co2Factor = 1.57;
export const nn2o_n2oFactor = 1.57;
// const nox_n2oFactor = 0.96;

export class GhgProtocol {
  organizationName: string;
  organizationAddress: string;
  responsablePersonName: string;
  inventaryYear: string;
  fillDate: string;
  sector: string;
  emissionFactors!: {
    stationaryFossilFuelFactors: Map<any, any>;
    stationaryBioFuelFactors: Map<any, any>;
    mobileRoadBioFuelFactors: Map<any, any>;
    mobileRoadFossilFuelFactors: Map<any, any>;
    mobileCoalRailFuelFactors: Map<any, any>;
    globalWarmingPotentialGasFactors: Map<any, any>;
    percentageMixedFuelByYear: Map<any, any>;
    finFactorsByYear: Map<any, any>;
    agriculturalActivityFertilizingFactors: Map<any, any>;
    agriculturalActivityGreenFertilizerFactors: Map<any, any>;
    agriculturalActivityCropResiduesDecompositionFactors: Map<any, any>;
    agriculturalActivityCropResiduesBurningFactors: Map<any, any>;
    agriculturalActivityChangeOfLandUseAndOccupationFactors: Map<any, any>;
    agriculturalActivityOrganicSoilCultivationFactors: Map<any, any>;
    agriculturalActivityIndirectEmissionsFactors: Map<any, any>;
    agriculturalActivitySecondaryEmissionsFactors: Map<any, any>;

    carbonStockNativeVegetationFactors: Map<any, any>;
  };
  consumptions: {
    // scope 1
    stationaryFuelConsumptions: Map<any, any>;
    mobileRoadFuelConsumptions: Map<any, any>;
    mobileRailFuelConsumptions: Map<any, any>;
    mobileHydroFuelConsumptions: Map<any, any>;
    mobileAirFuelConsumptions: Map<any, any>;
    fugitiveGasConsumptions: Map<any, any>;
    fugitiveSF6andNF3Consumptions: Map<any, any>;
    fugitiveOtherSourceToolsGasConsumptions: Map<any, any>;
    industrialProcessGasConsumptions: Map<any, any>;
    agriculturalActivitiesGasConsumptions: Map<any, any>;
    agriculturalActivityFertilizingConsumptions: Map<any, any>;
    agriculturalActivityLimingConsumptions: Map<any, any>;
    agriculturalActivityUreaConsumptions: Map<any, any>;
    agriculturalActivitySyntheticFertilizerConsumptions: Map<any, any>;
    agriculturalActivityOrganicFertilizerConsumptions: Map<any, any>;
    agriculturalActivityGreenFertilizerConsumptions: Map<any, any>;
    agriculturalActivityManagementCultureConsumptions: Map<any, any>;

    carbonStockNativeVegetation: Map<any, any>;
    soilChangeGasConsumptions: Map<any, any>;
    powerConsumption: Map<any, any>;
    scope3Cat1Consumption: Map<any, any>;
    scope3Cat2Consumption: Map<any, any>;
    scope3Cat3Consumption: Map<any, any>;
    scope3Cat4Consumption: Map<any, any>;
    scope3Cat5Consumption: Map<any, any>;
    scope3Cat6Consumption: Map<any, any>;
    scope3Cat7Consumption: Map<any, any>;
    scope3Cat8Consumption: Map<any, any>;
    scope3Cat9Consumption: Map<any, any>;
    scope3Cat10Consumption: Map<any, any>;
    scope3Cat11Consumption: Map<any, any>;
    scope3Cat12Consumption: Map<any, any>;
    scope3Cat13Consumption: Map<any, any>;
    scope3Cat14Consumption: Map<any, any>;
    scope3Cat15Consumption: Map<any, any>;
  };

  constructor(
    organizationName: string,
    organizationAddress: string,
    responsablePersonName: string,
    sector: string,
  ) {
    this.organizationName = organizationName;
    this.organizationAddress = organizationAddress;
    this.inventaryYear = '2022';
    this.responsablePersonName = responsablePersonName;
    this.fillDate = '01/10/2023';
    this.sector = sector;

    // @ts-expect-error I do not want to set the emissionFactors elements now.
    this.emissionFactors = {};
    this.loadEmissionFactors();

    this.consumptions = {
      //
      // scope 1
      //
      stationaryFuelConsumptions: new Map(),
      mobileRoadFuelConsumptions: new Map(),
      mobileRailFuelConsumptions: new Map(),
      mobileHydroFuelConsumptions: new Map(),
      mobileAirFuelConsumptions: new Map(),
      fugitiveGasConsumptions: new Map(),
      fugitiveSF6andNF3Consumptions: new Map(),
      fugitiveOtherSourceToolsGasConsumptions: new Map(),
      industrialProcessGasConsumptions: new Map(),
      soilChangeGasConsumptions: new Map(),
      // scope 1 - Agricultural
      agriculturalActivityLimingConsumptions: new Map(),
      agriculturalActivityUreaConsumptions: new Map(),
      agriculturalActivitySyntheticFertilizerConsumptions: new Map(),
      agriculturalActivityOrganicFertilizerConsumptions: new Map(),
      agriculturalActivityGreenFertilizerConsumptions: new Map(),
      agriculturalActivityManagementCultureConsumptions: new Map(),

      // Generic, you should not use
      agriculturalActivityFertilizingConsumptions: new Map(),
      agriculturalActivitiesGasConsumptions: new Map(),

      //
      // scope 2
      //
      powerConsumption: new Map(),
      // scope 3
      scope3Cat1Consumption: new Map(),
      scope3Cat2Consumption: new Map(),
      scope3Cat3Consumption: new Map(),
      scope3Cat4Consumption: new Map(),
      scope3Cat5Consumption: new Map(),
      scope3Cat6Consumption: new Map(),
      scope3Cat7Consumption: new Map(),
      scope3Cat8Consumption: new Map(),
      scope3Cat9Consumption: new Map(),
      scope3Cat10Consumption: new Map(),
      scope3Cat11Consumption: new Map(),
      scope3Cat12Consumption: new Map(),
      scope3Cat13Consumption: new Map(),
      scope3Cat14Consumption: new Map(),
      scope3Cat15Consumption: new Map(),

      // Not scopes
      carbonStockNativeVegetation: new Map(),
    };
  }

  // precisa tratar as excessões.
  loadEmissionFactors() {
    this.emissionFactors.stationaryFossilFuelFactors = new Map(
      Object.entries(stationaryFossilFuelFactorsJSON),
    );

    this.emissionFactors.stationaryBioFuelFactors = new Map(
      Object.entries(stationaryBioFuelFactorsJSON),
    );

    this.emissionFactors.mobileRoadBioFuelFactors = new Map(
      Object.entries(mobileRoadBioFuelFactorsJSON),
    );

    this.emissionFactors.mobileRoadFossilFuelFactors = new Map(
      Object.entries(mobileRoadFossilFuelFactorsJSON),
    );

    this.emissionFactors.mobileCoalRailFuelFactors = new Map(
      Object.entries(mobileCoalRailFuelFactorsJSON),
    );

    this.emissionFactors.globalWarmingPotentialGasFactors = new Map(
      Object.entries(globalWarmingPotentialGasFactorsJSON),
    );

    this.emissionFactors.percentageMixedFuelByYear = new Map(
      Object.entries(percentageMixedFuelByYearJSON),
    );

    this.emissionFactors.finFactorsByYear = new Map(
      Object.entries(finFactorsByYearJSON),
    );

    this.emissionFactors.agriculturalActivityFertilizingFactors = new Map(
      Object.entries(agriculturalActivityFertilizingFactorsJSON),
    );

    this.emissionFactors.agriculturalActivityGreenFertilizerFactors = new Map(
      Object.entries(agriculturalActivityGreenFertilizerFactorsJSON),
    );

    this.emissionFactors.agriculturalActivityCropResiduesDecompositionFactors =
      new Map(
        Object.entries(
          agriculturalActivityCropResiduesDecompositionFactorsJSON,
        ),
      );

    this.emissionFactors.agriculturalActivityCropResiduesBurningFactors =
      new Map(
        Object.entries(agriculturalActivityCropResiduesBurningFactorsJSON),
      );

    this.emissionFactors.agriculturalActivityChangeOfLandUseAndOccupationFactors =
      new Map(
        Object.entries(
          agriculturalActivityChangeOfLandUseAndOccupationFactorsJSON,
        ),
      );

    this.emissionFactors.carbonStockNativeVegetationFactors = new Map(
      Object.entries(carbonStockNativeVegetationFactorsJSON),
    );

    this.emissionFactors.agriculturalActivityOrganicSoilCultivationFactors =
      new Map(
        Object.entries(agriculturalActivityOrganicSoilCultivationFactorsJSON),
      );

    this.emissionFactors.agriculturalActivityIndirectEmissionsFactors = new Map(
      Object.entries(agriculturalActivityIndirectEmissionsFactorsJSON),
    );

    this.emissionFactors.agriculturalActivitySecondaryEmissionsFactors =
      new Map(
        Object.entries(agriculturalActivitySecondaryEmissionsFactorsJSON),
      );
  }
  //
  // Scope 1
  //

  // Stationary
  setStationaryFuelConsumption = setStationaryFuelConsumption;
  calcStationaryEmissions = calcStationaryEmissions;

  // Mobile
  setMobileRoadFuelConsumption = setMobileRoadFuelConsumption;
  calcMobileRoadEmissions = calcMobileRoadEmissions;

  setMobileRailFuelConsumption = setMobileRailFuelConsumption;
  calcMobileRailEmissions = calcMobileRailEmissions;

  setMobileHydroFuelConsumption = setMobileHydroFuelConsumption;
  calcMobileHydroEmissions = calcMobileHydroEmissions;

  setMobileAirFuelConsumption = setMobileAirFuelConsumption;
  calcMobileAirEmissions = calcMobileAirEmissions;

  //  TODO: the function should not be exported, it should be used only internally, but it's necessary to re-implement it.
  partialCalcMobileMixedFuelEmissions = partialCalcMobileMixedFuelEmissions;

  //fugitive
  setFugitiveGasConsumption = setFugitiveGasConsumption;
  calcFugitiveGasEmissions = calcFugitiveGasEmissions;
  setFugitiveSF6andNF3Consumption = setFugitiveSF6andNF3Consumption;
  calcSf6andNf3FugitiveGasEmissions = calcSf6andNf3FugitiveGasEmissions;
  setFugitiveOtherSourceToolsGasConsumption =
    setFugitiveOtherSourceToolsGasConsumption;
  calcFugitiveOtherSourceToolsGasEmissions =
    calcFugitiveOtherSourceToolsGasEmissions;

  setIndustrialProcessGasConsumption(
    description: string,
    {gasName, quantity }: any,
  ) {
    if (this.emissionFactors.globalWarmingPotentialGasFactors.has(gasName)) {
      this.consumptions.industrialProcessGasConsumptions.set(description, {
        gasName,
        quantity,
      });
      return true;
    } else {
      return false;
    }
  }

  // Agriculture Activity
  setAgriculturalActivityManagementCulture =
    setAgriculturalActivityManagementCulture;

  calcAgriculturalActivityManagementCultureLandUseEmissions =
    calcAgriculturalActivityManagementCultureLandUseEmissions;

  setAgriculturalActivityLimingConsumption =
    setAgriculturalActivityLimingConsumption;

  calcAgriculturalActivityLimingEmissions =
    calcAgriculturalActivityLimingEmissions;

  setAgriculturalActivityUreaConsumption =
    setAgriculturalActivityUreaConsumption;

  calcAgriculturalActivityUreaEmissions = calcAgriculturalActivityUreaEmissions;

  setAgriculturalActivitySyntheticFertilizerConsumption =
    setAgriculturalActivitySyntheticFertilizerConsumption;

  calcAgriculturalActivitySyntheticFertilizerEmissions =
    calcAgriculturalActivitySyntheticFertilizerEmissions;

  setAgriculturalActivityOrganicFertilizerConsumption =
    setAgriculturalActivityOrganicFertilizerConsumption;
  calcAgriculturalActivityOrganicFertilizerEmissions =
    calcAgriculturalActivityOrganicFertilizerEmissions;

  setAgriculturalActivityGreenFertilizerConsumption =
    setAgriculturalActivityGreenFertilizerConsumption;
  calcAgriculturalActivityGreenFertilizerEmissions =
    calcAgriculturalActivityGreenFertilizerEmissions;

  calcAgriculturalActivityCropResiduesDecompositionEmissions =
    calcAgriculturalActivityCropResiduesDecompositionEmissions;
  calcAgriculturalActivityCropResiduesBurningEmissions =
    calcAgriculturalActivityCropResiduesBurningEmissions;

  setAgriculturalActivityManagementCultureLandUse =
    setAgriculturalActivityManagementCultureLandUse;

  calcAgriculturalActivityManagementCultureOrganicSoilEmissions =
    calcAgriculturalActivityManagementCultureOrganicSoilEmissions;

  calcAgriculturalActivityAtmosphericDepositionEmission =
    calcAgriculturalActivityAtmosphericDepositionEmission;

  setAgriculturalActivityGasConsumption(
    description: string,
    { gasName, quantity }: any,
  ) {
    if (this.emissionFactors.globalWarmingPotentialGasFactors.has(gasName)) {
      this.consumptions.agriculturalActivitiesGasConsumptions.set(description, {
        gasName,
        quantity,
      });
      return true;
    } else {
      return false;
    }
  }

  setCarbonStockNativeVegetation(
    description: string,
    { nativeVegetationName, area }: any,
  ) {
    if (
      this.emissionFactors.carbonStockNativeVegetationFactors.has(
        nativeVegetationName,
      )
    ) {
      this.consumptions.carbonStockNativeVegetation.set(description, {
        nativeVegetationName,
        area,
      });
      return true;
    } else {
      return false;
    }
  }

  setSoilChangeGasConsumption(
    description: string,
    { gasName, quantity }: any,
  ) {
    if (this.emissionFactors.globalWarmingPotentialGasFactors.has(gasName)) {
      this.consumptions.soilChangeGasConsumptions.set(description, {
        gasName,
        quantity,
      });
      return true;
    } else {
      return false;
    }
  }

  calcIndustrialProcessEmissions() {
    return this.calcGenericEmissions(
      this.consumptions.industrialProcessGasConsumptions,
      this.emissionFactors.globalWarmingPotentialGasFactors,
    );
  }

  calcAgriculturalActivityEmissions() {
    return this.calcGenericEmissions(
      this.consumptions.agriculturalActivitiesGasConsumptions,
      this.emissionFactors.globalWarmingPotentialGasFactors,
    );
  }

  calcSoilChangeActivityEmissions() {
    return this.calcTonGenericEmissions(
      this.consumptions.soilChangeGasConsumptions,
      this.emissionFactors.globalWarmingPotentialGasFactors,
    );
  }

  calcGenericEmissions(
    consumptions: any[] | Map<any, any>,
    factors: Map<any, any>,
  ) {
    let tonCo2Eq = 0;
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    consumptions.forEach((values, _key) => {
      tonCo2Eq += (values.quantity * factors.get(values.gasName)) / 1000;
    });
    return {
      tonCo2Eq: tonCo2Eq,
    };
  }

  calcTonGenericEmissions(
    consumptions: any[] | Map<any, any>,
    factors: Map<any, any>,
  ) {
    let tonCo2Eq = 0;
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    consumptions.forEach((values, _key) => {
      tonCo2Eq += values.quantity * factors.get(values.gasName);
    });
    return {
      tonCo2Eq: tonCo2Eq,
    };
  }

  calcCarbonStockNativeVegetation() {
    let tonCo2 = 0;
    const tonCh4 = 0;
    const tonN2o = 0;
    let tonCo2Eq = 0;
    const tonCo2Bio = 0;
    let emissionFactor;

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    this.consumptions.carbonStockNativeVegetation.forEach((values, _key) => {
      if (
        this.emissionFactors.carbonStockNativeVegetationFactors.has(
          values.nativeVegetationName,
        )
      ) {
        emissionFactor =
          this.emissionFactors.carbonStockNativeVegetationFactors.get(
            values.nativeVegetationName,
          );
        tonCo2 += values.area * emissionFactor.co2Factor;
      }
    });

    tonCo2Eq +=
      tonCo2 *
      this.emissionFactors.globalWarmingPotentialGasFactors.get(
        'Dióxido de carbono (CO2)',
      );

    return {
      tonCo2: tonCo2,
      tonCh4: tonCh4,
      tonN2o: tonN2o,
      tonCo2Eq: tonCo2Eq,
      tonCo2Bio: tonCo2Bio,
    };
  }

  //
  // Scope 2
  //

  setPowerConsumption = setPowerConsumption;
  calcPowerEmissions = calcPowerEmissions;

  //
  // Scope 3
  //

  setCat1Consumption = setCat1Consumption;
  calcCat1Emissions = calcCat1Emissions;
  setCat2Consumption = setCat2Consumption;
  calcCat2Emissions = calcCat2Emissions;
}

/*
function emissionZero() {
  return {
    tonCo2: 0,
    tonCh4: 0,
    tonN2o: 0,
    tonCo2Eq: 0,
    tonCo2Bio: 0,
  };
}
*/
// exports = { GhgProtocol };
