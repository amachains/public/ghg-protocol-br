/* eslint-disable @typescript-eslint/no-explicit-any */
import { GhgProtocol } from './ghgProtocol';

export function setCat1Consumption(
  this: GhgProtocol,
  description: string,
  { gasName, quantity }: any,
) {
  if (this.emissionFactors.globalWarmingPotentialGasFactors.has(gasName)) {
    this.consumptions.scope3Cat1Consumption.set(description, {
      gasName,
      quantity,
    });
    return true;
  } else {
    return false;
  }
}

export function calcCat1Emissions(this: GhgProtocol) {
  let tonCo2Eq = 0;
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  this.consumptions.scope3Cat1Consumption.forEach((values, _key) => {
    tonCo2Eq +=
      values.quantity *
      this.emissionFactors.globalWarmingPotentialGasFactors.get(values.gasName);
  });
  return {
    tonCo2Eq: tonCo2Eq,
  };
}

export function setCat2Consumption(
  this: GhgProtocol,
  description: string,
  { gasName, quantity }: any,
) {
  if (this.emissionFactors.globalWarmingPotentialGasFactors.has(gasName)) {
    this.consumptions.scope3Cat2Consumption.set(description, {
      gasName,
      quantity,
    });
    return true;
  } else {
    return false;
  }
}

export function calcCat2Emissions(this: GhgProtocol) {
  let tonCo2Eq = 0;
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  this.consumptions.scope3Cat2Consumption.forEach((values, _key) => {
    tonCo2Eq +=
      values.quantity *
      this.emissionFactors.globalWarmingPotentialGasFactors.get(values.gasName);
  });
  return {
    tonCo2Eq: tonCo2Eq,
  };
}
