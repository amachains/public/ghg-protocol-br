/* eslint-disable @typescript-eslint/no-explicit-any */
import { GhgProtocol } from './ghgProtocol';

export function setStationaryFuelConsumption(
  this: GhgProtocol,
  description: string,
  { fuelName, quantity }: any,
) {
  if (this.emissionFactors.percentageMixedFuelByYear.has(fuelName)) {
    this.consumptions.stationaryFuelConsumptions.set(description, {
      fuelName,
      quantity,
    });
    return true;
  } else if (this.emissionFactors.stationaryFossilFuelFactors.has(fuelName)) {
    this.consumptions.stationaryFuelConsumptions.set(description, {
      fuelName,
      quantity,
    });
    return true;
  } else if (this.emissionFactors.stationaryBioFuelFactors.has(fuelName)) {
    this.consumptions.stationaryFuelConsumptions.set(description, {
      fuelName,
      quantity,
    });
    return true;
  } else {
    return false;
  }
}

export function calcStationaryEmissions(this: GhgProtocol) {
  let tonCo2 = 0;
  let tonCh4 = 0;
  let tonN2o = 0;
  let tonCo2Eq = 0;
  let tonCo2Bio = 0;
  let fuelEmissionFactor;

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  this.consumptions.stationaryFuelConsumptions.forEach((values, _key) => {
    if (this.emissionFactors.percentageMixedFuelByYear.has(values.fuelName)) {
      const mixedFuel = this.emissionFactors.percentageMixedFuelByYear.get(
        values.fuelName,
      )[this.inventaryYear];

      // mixed fuel situation
      for (const [mixedFuelName, mixedFuelPercentage] of Object.entries(
        mixedFuel,
      )) {
        // calculo de Co2 biogenico e fossil
        if (this.emissionFactors.stationaryBioFuelFactors.has(mixedFuelName)) {
          fuelEmissionFactor =
            this.emissionFactors.stationaryBioFuelFactors.get(mixedFuelName);
          tonCo2Bio +=
            // @ts-expect-error i did not defined mixedFuelPercentage
            (((values.quantity * mixedFuelPercentage) / 100) *
              fuelEmissionFactor.co2Factor) /
            1000;
        } else {
          fuelEmissionFactor =
            this.emissionFactors.stationaryFossilFuelFactors.get(mixedFuelName);
          tonCo2 +=
            // @ts-expect-error i did not defined mixedFuelPercentage
            (((values.quantity * mixedFuelPercentage) / 100) *
              fuelEmissionFactor.co2Factor) /
            1000;
        }
        tonCh4 +=
          // @ts-expect-error i did not defined mixedFuelPercentage
          (((values.quantity * mixedFuelPercentage) / 100) *
            fuelEmissionFactor.ch4Factor[this.sector]) /
          1000;
        tonN2o +=
          // @ts-expect-error i did not defined mixedFuelPercentage
          (((values.quantity * mixedFuelPercentage) / 100) *
            fuelEmissionFactor.n2oFactor[this.sector]) /
          1000;
      }
    } else if (
      this.emissionFactors.stationaryFossilFuelFactors.has(values.fuelName)
    ) {
      fuelEmissionFactor = this.emissionFactors.stationaryFossilFuelFactors.get(
        values.fuelName,
      );
      tonCo2 += (values.quantity * fuelEmissionFactor.co2Factor) / 1000;
      tonCh4 +=
        (values.quantity * fuelEmissionFactor.ch4Factor[this.sector]) / 1000;
      tonN2o +=
        (values.quantity * fuelEmissionFactor.n2oFactor[this.sector]) / 1000;
    } else if (
      this.emissionFactors.stationaryBioFuelFactors.has(values.fuelName)
    ) {
      fuelEmissionFactor = this.emissionFactors.stationaryBioFuelFactors.get(
        values.fuelName,
      );
      tonCo2Bio += (values.quantity * fuelEmissionFactor.co2Factor) / 1000;
      tonCh4 +=
        (values.quantity * fuelEmissionFactor.ch4Factor[this.sector]) / 1000;
      tonN2o +=
        (values.quantity * fuelEmissionFactor.n2oFactor[this.sector]) / 1000;
    }
  });

  tonCo2Eq +=
    tonCo2 *
    this.emissionFactors.globalWarmingPotentialGasFactors.get(
      'Dióxido de carbono (CO2)',
    );
  tonCo2Eq +=
    tonCh4 *
    this.emissionFactors.globalWarmingPotentialGasFactors.get('Metano (CH4)');
  tonCo2Eq +=
    tonN2o *
    this.emissionFactors.globalWarmingPotentialGasFactors.get(
      'Óxido nitroso (N2O)',
    );

  return {
    tonCo2: tonCo2,
    tonCh4: tonCh4,
    tonN2o: tonN2o,
    tonCo2Eq: tonCo2Eq,
    tonCo2Bio: tonCo2Bio,
  };
}
