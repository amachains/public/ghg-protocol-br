/* eslint-disable @typescript-eslint/no-explicit-any */
import { GhgProtocol } from './ghgProtocol';

export function setMobileRoadFuelConsumption(
  this: GhgProtocol,
  description: string,
  { fuelName, quantity }: any,
) {
  if (this.emissionFactors.percentageMixedFuelByYear.has(fuelName)) {
    this.consumptions.mobileRoadFuelConsumptions.set(description, {
      fuelName,
      quantity,
    });
    return true;
  } else if (this.emissionFactors.mobileRoadBioFuelFactors.has(fuelName)) {
    this.consumptions.mobileRoadFuelConsumptions.set(description, {
      fuelName,
      quantity,
    });
    return true;
  } else if (this.emissionFactors.mobileRoadFossilFuelFactors.has(fuelName)) {
    this.consumptions.mobileRoadFuelConsumptions.set(description, {
      fuelName,
      quantity,
    });
    return true;
  } else {
    return false;
  }
}

export function calcMobileRoadEmissions(this: GhgProtocol) {
  let tonCo2 = 0;
  let tonCh4 = 0;
  let tonN2o = 0;
  let tonCo2Eq = 0;
  let tonCo2Bio = 0;
  let fuelEmissionFactor;
  let partialCalcResults;
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  this.consumptions.mobileRoadFuelConsumptions.forEach((values, _key) => {
    partialCalcResults = this.partialCalcMobileMixedFuelEmissions(values);
    tonCo2 += partialCalcResults.tonCo2;
    tonCh4 += partialCalcResults.tonCh4;
    tonN2o += partialCalcResults.tonN2o;
    tonCo2Bio += partialCalcResults.tonCo2Bio;

    if (this.emissionFactors.mobileRoadFossilFuelFactors.has(values.fuelName)) {
      fuelEmissionFactor = this.emissionFactors.mobileRoadFossilFuelFactors.get(
        values.fuelName,
      );
      tonCo2 += (values.quantity * fuelEmissionFactor.co2Factor) / 1000;
      tonCh4 += (values.quantity * fuelEmissionFactor.ch4Factor) / 1000;
      tonN2o += (values.quantity * fuelEmissionFactor.n2oFactor) / 1000;
    } else if (
      this.emissionFactors.mobileRoadBioFuelFactors.has(values.fuelName)
    ) {
      fuelEmissionFactor = this.emissionFactors.mobileRoadBioFuelFactors.get(
        values.fuelName,
      );
      tonCo2Bio += (values.quantity * fuelEmissionFactor.co2Factor) / 1000;
      tonCh4 += (values.quantity * fuelEmissionFactor.ch4Factor) / 1000;
      tonN2o += (values.quantity * fuelEmissionFactor.n2oFactor) / 1000;
    }
  });

  tonCo2Eq +=
    tonCo2 *
    this.emissionFactors.globalWarmingPotentialGasFactors.get(
      'Dióxido de carbono (CO2)',
    );
  tonCo2Eq +=
    tonCh4 *
    this.emissionFactors.globalWarmingPotentialGasFactors.get('Metano (CH4)');
  tonCo2Eq +=
    tonN2o *
    this.emissionFactors.globalWarmingPotentialGasFactors.get(
      'Óxido nitroso (N2O)',
    );

  return {
    tonCo2: tonCo2,
    tonCh4: tonCh4,
    tonN2o: tonN2o,
    tonCo2Eq: tonCo2Eq,
    tonCo2Bio: tonCo2Bio,
  };
}

export function setMobileRailFuelConsumption(
  this: GhgProtocol,
  description: string,
  { fuelName, quantity }: any,
) {
  if (this.emissionFactors.percentageMixedFuelByYear.has(fuelName)) {
    this.consumptions.mobileRailFuelConsumptions.set(description, {
      fuelName,
      quantity,
    });
    return true;
  } else if (this.emissionFactors.mobileRoadBioFuelFactors.has(fuelName)) {
    this.consumptions.mobileRailFuelConsumptions.set(description, {
      fuelName,
      quantity,
    });
    return true;
  } else if (this.emissionFactors.mobileRoadFossilFuelFactors.has(fuelName)) {
    this.consumptions.mobileRailFuelConsumptions.set(description, {
      fuelName,
      quantity,
    });
    return true;
  } else if (this.emissionFactors.mobileCoalRailFuelFactors.has(fuelName)) {
    this.consumptions.mobileRailFuelConsumptions.set(description, {
      fuelName,
      quantity,
    });
    return true;
  } else {
    return false;
  }
}

export function calcMobileRailEmissions(this: GhgProtocol) {
  let tonCo2 = 0;
  let tonCh4 = 0;
  let tonN2o = 0;
  let tonCo2Eq = 0;
  let tonCo2Bio = 0;
  let fuelEmissionFactor;
  let partialCalcResults;
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  this.consumptions.mobileRailFuelConsumptions.forEach((values, _key) => {
    partialCalcResults = this.partialCalcMobileMixedFuelEmissions(values);
    tonCo2 += partialCalcResults.tonCo2;
    tonCh4 += partialCalcResults.tonCh4;
    tonN2o += partialCalcResults.tonN2o;
    tonCo2Bio += partialCalcResults.tonCo2Bio;

    if (this.emissionFactors.mobileRoadFossilFuelFactors.has(values.fuelName)) {
      fuelEmissionFactor = this.emissionFactors.mobileRoadFossilFuelFactors.get(
        values.fuelName,
      );
      tonCo2 += (values.quantity * fuelEmissionFactor.co2Factor) / 1000;
      tonCh4 += (values.quantity * fuelEmissionFactor.ch4Factor) / 1000;
      tonN2o += (values.quantity * fuelEmissionFactor.n2oFactor) / 1000;
    } else if (
      this.emissionFactors.mobileRoadBioFuelFactors.has(values.fuelName)
    ) {
      fuelEmissionFactor = this.emissionFactors.mobileRoadBioFuelFactors.get(
        values.fuelName,
      );
      tonCo2Bio += (values.quantity * fuelEmissionFactor.co2Factor) / 1000;
      tonCh4 += (values.quantity * fuelEmissionFactor.ch4Factor) / 1000;
      tonN2o += (values.quantity * fuelEmissionFactor.n2oFactor) / 1000;
    } else if (
      this.emissionFactors.mobileCoalRailFuelFactors.has(values.fuelName)
    ) {
      fuelEmissionFactor = this.emissionFactors.mobileCoalRailFuelFactors.get(
        values.fuelName,
      );
      tonCo2 += (values.quantity * fuelEmissionFactor.co2Factor) / 1000;
      tonCh4 += (values.quantity * fuelEmissionFactor.ch4Factor) / 1000;
      tonN2o += (values.quantity * fuelEmissionFactor.n2oFactor) / 1000;
    }
  });

  tonCo2Eq +=
    tonCo2 *
    this.emissionFactors.globalWarmingPotentialGasFactors.get(
      'Dióxido de carbono (CO2)',
    );
  tonCo2Eq +=
    tonCh4 *
    this.emissionFactors.globalWarmingPotentialGasFactors.get('Metano (CH4)');
  tonCo2Eq +=
    tonN2o *
    this.emissionFactors.globalWarmingPotentialGasFactors.get(
      'Óxido nitroso (N2O)',
    );

  return {
    tonCo2: tonCo2,
    tonCh4: tonCh4,
    tonN2o: tonN2o,
    tonCo2Eq: tonCo2Eq,
    tonCo2Bio: tonCo2Bio,
  };
}

export function setMobileHydroFuelConsumption(
  this: GhgProtocol,
  description: string,
  { fuelName, quantity }: any,
) {
  if (this.emissionFactors.percentageMixedFuelByYear.has(fuelName)) {
    this.consumptions.mobileHydroFuelConsumptions.set(description, {
      fuelName,
      quantity,
    });
    return true;
  } else if (this.emissionFactors.mobileRoadBioFuelFactors.has(fuelName)) {
    this.consumptions.mobileHydroFuelConsumptions.set(description, {
      fuelName,
      quantity,
    });
    return true;
  } else if (this.emissionFactors.mobileRoadFossilFuelFactors.has(fuelName)) {
    this.consumptions.mobileHydroFuelConsumptions.set(description, {
      fuelName,
      quantity,
    });
    return true;
  } else {
    return false;
  }
}

export function calcMobileHydroEmissions(this: GhgProtocol) {
  let tonCo2 = 0;
  let tonCh4 = 0;
  let tonN2o = 0;
  let tonCo2Eq = 0;
  let tonCo2Bio = 0;
  let fuelEmissionFactor;
  let partialCalcResults;
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  this.consumptions.mobileHydroFuelConsumptions.forEach((values, _key) => {
    partialCalcResults = this.partialCalcMobileMixedFuelEmissions(values);
    tonCo2 += partialCalcResults.tonCo2;
    tonCh4 += partialCalcResults.tonCh4;
    tonN2o += partialCalcResults.tonN2o;
    tonCo2Bio += partialCalcResults.tonCo2Bio;

    if (this.emissionFactors.mobileRoadFossilFuelFactors.has(values.fuelName)) {
      fuelEmissionFactor = this.emissionFactors.mobileRoadFossilFuelFactors.get(
        values.fuelName,
      );
      tonCo2 += (values.quantity * fuelEmissionFactor.co2Factor) / 1000;
      tonCh4 += (values.quantity * fuelEmissionFactor.ch4Factor) / 1000;
      tonN2o += (values.quantity * fuelEmissionFactor.n2oFactor) / 1000;
    } else if (
      this.emissionFactors.mobileRoadBioFuelFactors.has(values.fuelName)
    ) {
      fuelEmissionFactor = this.emissionFactors.mobileRoadBioFuelFactors.get(
        values.fuelName,
      );
      tonCo2Bio += (values.quantity * fuelEmissionFactor.co2Factor) / 1000;
      tonCh4 += (values.quantity * fuelEmissionFactor.ch4Factor) / 1000;
      tonN2o += (values.quantity * fuelEmissionFactor.n2oFactor) / 1000;
    }
  });

  tonCo2Eq +=
    tonCo2 *
    this.emissionFactors.globalWarmingPotentialGasFactors.get(
      'Dióxido de carbono (CO2)',
    );
  tonCo2Eq +=
    tonCh4 *
    this.emissionFactors.globalWarmingPotentialGasFactors.get('Metano (CH4)');
  tonCo2Eq +=
    tonN2o *
    this.emissionFactors.globalWarmingPotentialGasFactors.get(
      'Óxido nitroso (N2O)',
    );

  return {
    tonCo2: tonCo2,
    tonCh4: tonCh4,
    tonN2o: tonN2o,
    tonCo2Eq: tonCo2Eq,
    tonCo2Bio: tonCo2Bio,
  };
}

export function setMobileAirFuelConsumption(
  this: GhgProtocol,
  description: string,
  { fuelName, quantity }: any,
) {
  if (this.emissionFactors.percentageMixedFuelByYear.has(fuelName)) {
    this.consumptions.mobileAirFuelConsumptions.set(description, {
      fuelName,
      quantity,
    });
    return true;
  } else if (this.emissionFactors.mobileRoadBioFuelFactors.has(fuelName)) {
    this.consumptions.mobileAirFuelConsumptions.set(description, {
      fuelName,
      quantity,
    });
    return true;
  } else if (this.emissionFactors.mobileRoadFossilFuelFactors.has(fuelName)) {
    this.consumptions.mobileAirFuelConsumptions.set(description, {
      fuelName,
      quantity,
    });
    return true;
  } else {
    return false;
  }
}

export function calcMobileAirEmissions(this: GhgProtocol) {
  let tonCo2 = 0;
  let tonCh4 = 0;
  let tonN2o = 0;
  let tonCo2Eq = 0;
  let fuelEmissionFactor;
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  this.consumptions.mobileAirFuelConsumptions.forEach((values, _key) => {
    if (this.emissionFactors.mobileRoadFossilFuelFactors.has(values.fuelName)) {
      fuelEmissionFactor = this.emissionFactors.mobileRoadFossilFuelFactors.get(
        values.fuelName,
      );
      tonCo2 += (values.quantity * fuelEmissionFactor.co2Factor) / 1000;
      tonCh4 += (values.quantity * fuelEmissionFactor.ch4Factor) / 1000;
      tonN2o += (values.quantity * fuelEmissionFactor.n2oFactor) / 1000;
    }
  });

  tonCo2Eq +=
    tonCo2 *
    this.emissionFactors.globalWarmingPotentialGasFactors.get(
      'Dióxido de carbono (CO2)',
    );
  tonCo2Eq +=
    tonCh4 *
    this.emissionFactors.globalWarmingPotentialGasFactors.get('Metano (CH4)');
  tonCo2Eq +=
    tonN2o *
    this.emissionFactors.globalWarmingPotentialGasFactors.get(
      'Óxido nitroso (N2O)',
    );

  return {
    tonCo2: tonCo2,
    tonCh4: tonCh4,
    tonN2o: tonN2o,
    tonCo2Eq: tonCo2Eq,
    tonCo2Bio: 0,
  };
}

export function partialCalcMobileMixedFuelEmissions(
  this: GhgProtocol,
  consumption: { fuelName: any; quantity: number },
) {
  let tonCo2 = 0;
  let tonCh4 = 0;
  let tonN2o = 0;
  let tonCo2Bio = 0;
  let fuelEmissionFactor;

  if (
    this.emissionFactors.percentageMixedFuelByYear.has(consumption.fuelName)
  ) {
    const mixedFuel = this.emissionFactors.percentageMixedFuelByYear.get(
      consumption.fuelName,
    )[this.inventaryYear];

    // mixed fuel situation
    for (const [mixedFuelName, mixedFuelPercentage] of Object.entries(
      mixedFuel,
    )) {
      // calculo de Co2 biogenico e fossil
      if (this.emissionFactors.mobileRoadBioFuelFactors.has(mixedFuelName)) {
        fuelEmissionFactor =
          this.emissionFactors.mobileRoadBioFuelFactors.get(mixedFuelName);
        tonCo2Bio +=
          // @ts-expect-error I did not defined mixedFuelPercentage
          (((consumption.quantity * mixedFuelPercentage) / 100) *
            fuelEmissionFactor.co2Factor) /
          1000;
      } else {
        fuelEmissionFactor =
          this.emissionFactors.mobileRoadFossilFuelFactors.get(mixedFuelName);
        tonCo2 +=
          // @ts-expect-error I did not defined mixedFuelPercentage
          (((consumption.quantity * mixedFuelPercentage) / 100) *
            fuelEmissionFactor.co2Factor) /
          1000;
      }
      tonCh4 +=
        // @ts-expect-error I did not defined mixedFuelPercentage
        (((consumption.quantity * mixedFuelPercentage) / 100) *
          fuelEmissionFactor.ch4Factor) /
        1000;
      tonN2o +=
        // @ts-expect-error I did not defined mixedFuelPercentage
        (((consumption.quantity * mixedFuelPercentage) / 100) *
          fuelEmissionFactor.n2oFactor) /
        1000;
    }
  }

  return {
    tonCo2: tonCo2,
    tonCh4: tonCh4,
    tonN2o: tonN2o,
    tonCo2Bio: tonCo2Bio,
  };
}
