/* eslint-disable @typescript-eslint/no-explicit-any */
import { GhgProtocol, nn2o_n2oFactor } from './ghgProtocol';
import { AGRICULTURAL_LIMINGSUBSTANCES } from './ghgProtocol';
import { AGRICULTURAL_ORGANIC_FERTILIZER } from './ghgProtocol';
import { AGRICULTURAL_CULTURES } from './ghgProtocol';
import { CROPS_WITH_SECONDARY_HARVEST } from './ghgProtocol';

export function setAgriculturalActivityManagementCulture(
  this: GhgProtocol,
  description: string,
  {
    cultureName,
    quantity,
    area,
    cropResidueManagement,
    organicSoil,
    mainCrop,
  }: any,
) {
  if (AGRICULTURAL_CULTURES.includes(cultureName)) {
    this.consumptions.agriculturalActivityManagementCultureConsumptions.set(
      description,
      {
        cultureName,
        quantity,
        area,
        cropResidueManagement,
        organicSoil,
        mainCrop,
      },
    );
    return true;
  } else {
    return false;
  }
}

export function calcAgriculturalActivityManagementCultureLandUseEmissions(
  this: GhgProtocol,
) {
  const tonCo2 = 0;
  const tonCh4 = 0;
  const tonN2o = 0;
  let tonCo2Eq = 0;
  const tonCo2Bio = 0;
  let emissionFactor;

  this.consumptions.agriculturalActivityManagementCultureConsumptions.forEach(
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    (values: any, _key: any) => {
      if (Object.prototype.hasOwnProperty.call(values, 'previousLandUse')) {
        if (
          values.transitionTimeOver20Years ||
          values.previousLandUse === values.currentLandUse
        ) {
          return null;
        }
        if (
          CROPS_WITH_SECONDARY_HARVEST.includes(values.cultureName) &&
          !values.mainCrop
        )
          return null;
        // TODO: Ajustar essa parte para o florestal, por enquanto não estou lidando com ele
        // if (CROPS_WITH_ICLF.includes(values.cultureName))
        //   return (null)
        emissionFactor =
          this.emissionFactors.agriculturalActivityChangeOfLandUseAndOccupationFactors.get(
            values.previousLandUse,
          )[values.currentLandUse];
        tonCo2Eq += values.area * emissionFactor.co2EqFactor;
      }
    },
  );
  return {
    tonCo2: tonCo2,
    tonCh4: tonCh4,
    tonN2o: tonN2o,
    tonCo2Eq: tonCo2Eq,
    tonCo2Bio: tonCo2Bio,
  };
}

export function calcAgriculturalActivityManagementCultureOrganicSoilEmissions(
  this: GhgProtocol,
) {
  let tonCo2 = 0;
  const tonCh4 = 0;
  const tonN2o = 0;
  let tonCo2Eq = 0;
  const tonCo2Bio = 0;
  let emissionFactor;

  this.consumptions.agriculturalActivityManagementCultureConsumptions.forEach(
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    (values: any, _key: any) => {
      if (values.organicSoil) {
        emissionFactor =
          this.emissionFactors.agriculturalActivityOrganicSoilCultivationFactors.get(
            'Cultivo de solos orgânicos',
          );
        tonCo2 += values.area * emissionFactor.co2Factor;
      }
    },
  );
  tonCo2Eq = tonCo2;
  return {
    tonCo2: tonCo2Eq,
    tonCh4: tonCh4,
    tonN2o: tonN2o,
    tonCo2Eq: tonCo2Eq,
    tonCo2Bio: tonCo2Bio,
  };
}

export function setAgriculturalActivityLimingConsumption(
  this: GhgProtocol,
  description: string,
  { managementCulture, substanceName, quantity }: any,
) {
  if (
    this.consumptions.agriculturalActivityManagementCultureConsumptions.has(
      managementCulture,
    )
  ) {
    if (AGRICULTURAL_LIMINGSUBSTANCES.includes(substanceName)) {
      this.consumptions.agriculturalActivityLimingConsumptions.set(
        description,
        { managementCulture, substanceName, quantity },
      );
      return true;
    } else {
      return false;
    }
  } else {
    return false;
  }
}

export function calcAgriculturalActivityLimingEmissions(this: GhgProtocol) {
  let tonCo2 = 0;
  let tonCh4 = 0;
  let tonN2o = 0;
  let tonCo2Eq = 0;
  const tonCo2Bio = 0;
  let emissionFactor;
  this.consumptions.agriculturalActivityLimingConsumptions.forEach(
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    (values, _key) => {
      if (
        this.emissionFactors.agriculturalActivityFertilizingFactors.has(
          values.substanceName,
        )
      ) {
        const managementCulture =
          this.consumptions.agriculturalActivityManagementCultureConsumptions.get(
            values.managementCulture,
          );
        emissionFactor =
          this.emissionFactors.agriculturalActivityFertilizingFactors.get(
            values.substanceName,
          );
        tonCo2 +=
          (values.quantity *
            managementCulture.area *
            emissionFactor.co2Factor) /
          1000;
        tonCh4 +=
          (values.quantity *
            managementCulture.area *
            emissionFactor.ch4Factor) /
          1000;
        tonN2o +=
          (values.quantity *
            managementCulture.area *
            emissionFactor.n2oFactor) /
          1000;
      }
    },
  );

  tonCo2Eq +=
    tonCo2 *
    this.emissionFactors.globalWarmingPotentialGasFactors.get(
      'Dióxido de carbono (CO2)',
    );
  tonCo2Eq +=
    tonCh4 *
    this.emissionFactors.globalWarmingPotentialGasFactors.get('Metano (CH4)');
  tonCo2Eq +=
    tonN2o *
    this.emissionFactors.globalWarmingPotentialGasFactors.get(
      'Óxido nitroso (N2O)',
    );

  return {
    tonCo2: tonCo2,
    tonCh4: tonCh4,
    tonN2o: tonN2o,
    tonCo2Eq: tonCo2Eq,
    tonCo2Bio: tonCo2Bio,
  };
}

export function setAgriculturalActivityUreaConsumption(
  this: GhgProtocol,
  description: string,
  { managementCulture, quantity }: any,
) {
  if (
    this.consumptions.agriculturalActivityManagementCultureConsumptions.has(
      managementCulture,
    )
  ) {
    return this.consumptions.agriculturalActivityUreaConsumptions.set(
      description,
      { managementCulture, substanceName: 'Ureia', quantity },
    );
  } else {
    return false;
  }
}

export function calcAgriculturalActivityUreaEmissions(this: GhgProtocol) {
  let tonCo2 = 0;
  let tonCh4 = 0;
  let tonN2o = 0;
  let tonCo2Eq = 0;
  const tonCo2Bio = 0;
  const emissionFactor =
    this.emissionFactors.agriculturalActivityFertilizingFactors.get('Ureia');

  this.consumptions.agriculturalActivityUreaConsumptions.forEach(
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    (values, _key) => {
      const managementCulture =
        this.consumptions.agriculturalActivityManagementCultureConsumptions.get(
          values.managementCulture,
        );
      tonCo2 +=
        (values.quantity * managementCulture.area * emissionFactor.co2Factor) /
        1000;
      tonCh4 +=
        (values.quantity * managementCulture.area * emissionFactor.ch4Factor) /
        1000;
      // 45/100 é o fator de nitrogenio na ureia
      tonN2o +=
        (values.quantity *
          managementCulture.area *
          emissionFactor.n2oFactor *
          45) /
        100 /
        1000;
    },
  );

  tonCo2Eq +=
    tonCo2 *
    this.emissionFactors.globalWarmingPotentialGasFactors.get(
      'Dióxido de carbono (CO2)',
    );
  tonCo2Eq +=
    tonCh4 *
    this.emissionFactors.globalWarmingPotentialGasFactors.get('Metano (CH4)');
  tonCo2Eq +=
    tonN2o *
    this.emissionFactors.globalWarmingPotentialGasFactors.get(
      'Óxido nitroso (N2O)',
    );

  return {
    tonCo2: tonCo2,
    tonCh4: tonCh4,
    tonN2o: tonN2o,
    tonCo2Eq: tonCo2Eq,
    tonCo2Bio: tonCo2Bio,
  };
}

export function setAgriculturalActivitySyntheticFertilizerConsumption(
  this: GhgProtocol,
  description: string,
  { managementCulture, quantity, percentageNitrogen }: any,
) {
  if (
    this.consumptions.agriculturalActivityManagementCultureConsumptions.has(
      managementCulture,
    )
  ) {
    return this.consumptions.agriculturalActivitySyntheticFertilizerConsumptions.set(
      description,
      {
        managementCulture,
        substanceName: 'Fertilizante sintético',
        quantity,
        percentageNitrogen,
      },
    );
  } else {
    return false;
  }
}

export function calcAgriculturalActivitySyntheticFertilizerEmissions(
  this: GhgProtocol,
) {
  let tonCo2 = 0;
  let tonCh4 = 0;
  let tonN2o = 0;
  let tonCo2Eq = 0;
  const tonCo2Bio = 0;
  const emissionFactor =
    this.emissionFactors.agriculturalActivityFertilizingFactors.get(
      'Fertilizante sintético',
    );

  this.consumptions.agriculturalActivitySyntheticFertilizerConsumptions.forEach(
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    (values, _key) => {
      const managementCulture =
        this.consumptions.agriculturalActivityManagementCultureConsumptions.get(
          values.managementCulture,
        );

      tonCo2 +=
        (values.quantity * managementCulture.area * emissionFactor.co2Factor) /
        1000;
      tonCh4 +=
        (values.quantity * managementCulture.area * emissionFactor.ch4Factor) /
        1000;
      tonN2o +=
        (values.quantity *
          managementCulture.area *
          emissionFactor.n2oFactor *
          values.percentageNitrogen) /
        100 /
        1000;
    },
  );

  tonCo2Eq +=
    tonCo2 *
    this.emissionFactors.globalWarmingPotentialGasFactors.get(
      'Dióxido de carbono (CO2)',
    );
  tonCo2Eq +=
    tonCh4 *
    this.emissionFactors.globalWarmingPotentialGasFactors.get('Metano (CH4)');
  tonCo2Eq +=
    tonN2o *
    this.emissionFactors.globalWarmingPotentialGasFactors.get(
      'Óxido nitroso (N2O)',
    );

  return {
    tonCo2: tonCo2,
    tonCh4: tonCh4,
    tonN2o: tonN2o,
    tonCo2Eq: tonCo2Eq,
    tonCo2Bio: tonCo2Bio,
  };
}

export function setAgriculturalActivityOrganicFertilizerConsumption(
  this: GhgProtocol,
  description: string,
  { managementCulture, substanceName, quantity }: any,
) {
  if (
    this.consumptions.agriculturalActivityManagementCultureConsumptions.has(
      managementCulture,
    )
  ) {
    if (AGRICULTURAL_ORGANIC_FERTILIZER.includes(substanceName)) {
      this.consumptions.agriculturalActivityOrganicFertilizerConsumptions.set(
        description,
        { managementCulture, substanceName, quantity },
      );
      return true;
    } else {
      return false;
    }
  } else {
    return false;
  }
}

export function calcAgriculturalActivityOrganicFertilizerEmissions(
  this: GhgProtocol,
) {
  let tonCo2 = 0;
  let tonCh4 = 0;
  let tonN2o = 0;
  let tonCo2Eq = 0;
  const tonCo2Bio = 0;
  let emissionFactor;

  this.consumptions.agriculturalActivityOrganicFertilizerConsumptions.forEach(
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    (values, _key) => {
      const managementCulture =
        this.consumptions.agriculturalActivityManagementCultureConsumptions.get(
          values.managementCulture,
        );
      emissionFactor =
        this.emissionFactors.agriculturalActivityFertilizingFactors.get(
          values.substanceName,
        );

      tonCo2 +=
        (values.quantity * managementCulture.area * emissionFactor.co2Factor) /
        1000;
      tonCh4 +=
        (values.quantity * managementCulture.area * emissionFactor.ch4Factor) /
        1000;
      tonN2o +=
        (values.quantity * managementCulture.area * emissionFactor.n2oFactor) /
        1000;
    },
  );

  tonCo2Eq +=
    tonCo2 *
    this.emissionFactors.globalWarmingPotentialGasFactors.get(
      'Dióxido de carbono (CO2)',
    );
  tonCo2Eq +=
    tonCh4 *
    this.emissionFactors.globalWarmingPotentialGasFactors.get('Metano (CH4)');
  tonCo2Eq +=
    tonN2o *
    this.emissionFactors.globalWarmingPotentialGasFactors.get(
      'Óxido nitroso (N2O)',
    );

  return {
    tonCo2: tonCo2,
    tonCh4: tonCh4,
    tonN2o: tonN2o,
    tonCo2Eq: tonCo2Eq,
    tonCo2Bio: tonCo2Bio,
  };
}

export function setAgriculturalActivityGreenFertilizerConsumption(
  this: GhgProtocol,
  description: string,
  { managementCulture, substanceName, quantity }: any,
) {
  if (
    this.consumptions.agriculturalActivityManagementCultureConsumptions.has(
      managementCulture,
    )
  ) {
    if (
      this.emissionFactors.agriculturalActivityGreenFertilizerFactors.has(
        substanceName,
      )
    ) {
      this.consumptions.agriculturalActivityGreenFertilizerConsumptions.set(
        description,
        { managementCulture, substanceName, quantity },
      );
      return true;
    } else {
      return false;
    }
  } else {
    return false;
  }
}

export function calcAgriculturalActivityGreenFertilizerEmissions(
  this: GhgProtocol,
) {
  const tonCo2 = 0;
  let tonCh4 = 0;
  let tonN2o = 0;
  let tonCo2Eq = 0;
  let tonCo2Bio = 0;
  let emissionFactor;
  const listManagementCulture: any[] = [];

  this.consumptions.agriculturalActivityGreenFertilizerConsumptions.forEach(
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    (values, _key) => {
      emissionFactor =
        this.emissionFactors.agriculturalActivityGreenFertilizerFactors.get(
          values.substanceName,
        );
      const managementCulture =
        this.consumptions.agriculturalActivityManagementCultureConsumptions.get(
          values.managementCulture,
        );
      if (!listManagementCulture.includes(values.managementCulture)) {
        // pega apenas a primeira aplicação de adubo verde e utiliza ela para calcular o V02 Biogenico
        tonCo2Bio += (managementCulture.area * emissionFactor.co2Factor) / 1000;
      }
      tonCh4 +=
        (values.quantity * managementCulture.area * emissionFactor.ch4Factor) /
        1000;
      tonN2o +=
        (values.quantity * managementCulture.area * emissionFactor.n2oFactor) /
        1000;
      listManagementCulture.push(values.managementCulture);
    },
  );

  tonCo2Eq +=
    tonCh4 *
    this.emissionFactors.globalWarmingPotentialGasFactors.get('Metano (CH4)');
  tonCo2Eq +=
    tonN2o *
    this.emissionFactors.globalWarmingPotentialGasFactors.get(
      'Óxido nitroso (N2O)',
    );

  return {
    tonCo2: tonCo2,
    tonCh4: tonCh4,
    tonN2o: tonN2o,
    tonCo2Eq: tonCo2Eq,
    tonCo2Bio: tonCo2Bio,
  };
}

export function calcAgriculturalActivityCropResiduesDecompositionEmissions(
  this: GhgProtocol,
) {
  const tonCo2 = 0;
  let tonCh4 = 0;
  let tonN2o = 0;
  let tonCo2Eq = 0;
  let tonCo2Bio = 0;
  let emissionFactor;

  this.consumptions.agriculturalActivityManagementCultureConsumptions.forEach(
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    (values: any, _key: string) => {
      if (values.cropResidueManagement == 'Decomposição') {
        emissionFactor =
          this.emissionFactors.agriculturalActivityCropResiduesDecompositionFactors.get(
            values.cultureName,
          );

        tonCo2Bio += values.quantity * values.area * emissionFactor.co2Factor;
        tonCh4 += values.quantity * values.area * emissionFactor.ch4Factor;
        tonN2o += values.quantity * values.area * emissionFactor.n2oFactor;
      }
    },
  );

  // tonCo2Eq += tonCo2 * this.emissionFactors.globalWarmingPotentialGasFactors.get('Dióxido de carbono (CO2)')
  tonCo2Eq +=
    tonCh4 *
    this.emissionFactors.globalWarmingPotentialGasFactors.get('Metano (CH4)');
  tonCo2Eq +=
    tonN2o *
    this.emissionFactors.globalWarmingPotentialGasFactors.get(
      'Óxido nitroso (N2O)',
    );

  return {
    tonCo2: tonCo2,
    tonCh4: tonCh4,
    tonN2o: tonN2o,
    tonCo2Eq: tonCo2Eq,
    tonCo2Bio: tonCo2Bio,
  };
}
// TODO: Simplificar essas funções criando uma generica e depois utilizando ela apenas mudando o paramentro de cropResidueManagement

export function calcAgriculturalActivityCropResiduesBurningEmissions(
  this: GhgProtocol,
) {
  const tonCo2 = 0;
  let tonCh4 = 0;
  let tonN2o = 0;
  let tonCo2Eq = 0;
  let tonCo2Bio = 0;
  let emissionFactor;

  this.consumptions.agriculturalActivityManagementCultureConsumptions.forEach(
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    (values: any, _key: string) => {
      if (values.cropResidueManagement == 'Queima') {
        emissionFactor =
          this.emissionFactors.agriculturalActivityCropResiduesBurningFactors.get(
            values.cultureName,
          );

        tonCo2Bio += values.quantity * values.area * emissionFactor.co2Factor;
        tonCh4 += values.quantity * values.area * emissionFactor.ch4Factor;
        tonN2o += values.quantity * values.area * emissionFactor.n2oFactor;
      }
    },
  );

  // tonCo2Eq += tonCo2 * this.emissionFactors.globalWarmingPotentialGasFactors.get('Dióxido de carbono (CO2)')
  tonCo2Eq +=
    tonCh4 *
    this.emissionFactors.globalWarmingPotentialGasFactors.get('Metano (CH4)');
  tonCo2Eq +=
    tonN2o *
    this.emissionFactors.globalWarmingPotentialGasFactors.get(
      'Óxido nitroso (N2O)',
    );

  return {
    tonCo2: tonCo2,
    tonCh4: tonCh4,
    tonN2o: tonN2o,
    tonCo2Eq: tonCo2Eq,
    tonCo2Bio: tonCo2Bio,
  };
}

export function setAgriculturalActivityManagementCultureLandUse(
  this: GhgProtocol,
  cultureDescription: string,
  {
    previousLandUse,
    currentLandUse,
    transitionTimeOver20Years,
    nativeVegetationSuppression,
    soilTexture,
    soilClayPercentage,
    soilCarbonPercentage,
    soilCarbonStock,
  }: any,
) {
  if (
    this.consumptions.agriculturalActivityManagementCultureConsumptions.has(
      cultureDescription,
    )
  ) {
    const currentConsumption =
      this.consumptions.agriculturalActivityManagementCultureConsumptions.get(
        cultureDescription,
      );

    const newConsumption = {
      previousLandUse,
      currentLandUse,
      transitionTimeOver20Years,
      nativeVegetationSuppression,
      soilTexture,
      soilClayPercentage,
      soilCarbonPercentage,
      soilCarbonStock,
    };

    const mergedConsuption = { ...currentConsumption, ...newConsumption };

    this.consumptions.agriculturalActivityManagementCultureConsumptions.set(
      cultureDescription,
      mergedConsuption,
    );
    return true;
  } else {
    return false;
  }
}

export function calcAgriculturalActivityAtmosphericDepositionEmission(
  this: GhgProtocol,
) {
  const tonCo2 = 0;
  const tonCh4 = 0;
  let tonN2o = 0;
  let tonCo2Eq = 0;
  const tonCo2Bio = 0;
  let syntheticFertilizer = 0;
  let organicFertilizer = 0;
  let managementCulture;

  const vdaFactor =
    this.emissionFactors.agriculturalActivitySecondaryEmissionsFactors.get(
      'Volatilização e deposição atmosférica',
    ).n2oFactor;
  const percentageVolaFactor =
    this.emissionFactors.agriculturalActivityIndirectEmissionsFactors.get(
      'Volatilização',
    ).percentage;
  const percentageResidueFactor =
    this.emissionFactors.agriculturalActivityIndirectEmissionsFactors.get(
      'Resíduo',
    ).percentage;

  const ureiaFactor =
    this.emissionFactors.agriculturalActivityFertilizingFactors.get('Ureia');

  this.consumptions.agriculturalActivitySyntheticFertilizerConsumptions.forEach(
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    (values, _key) => {
      managementCulture =
        this.consumptions.agriculturalActivityManagementCultureConsumptions.get(
          values.managementCulture,
        );

      syntheticFertilizer +=
        ((values.quantity * values.percentageNitrogen) / 100) *
        managementCulture.area;
    },
  );

  // ureia é um subtipo de fertilizante sintético por isso deve ser somado
  this.consumptions.agriculturalActivityUreaConsumptions.forEach(
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    (values, _key) => {
      managementCulture =
        this.consumptions.agriculturalActivityManagementCultureConsumptions.get(
          values.managementCulture,
        );

      syntheticFertilizer +=
        ((values.quantity * ureiaFactor.nitrogenPercentage) / 100) *
        ureiaFactor.n2oFactor *
        managementCulture.area;
    },
  );

  // multiplicar pelo fator Volatilização e deposição atmosférica ==> c_n2o_ef3 secundario
  // multiplicar pelo percentual de Volatilização / 100  ==> c_fracgasf indireto
  // multi pelo fator de conversao nn2o para n2o

  syntheticFertilizer =
    ((syntheticFertilizer * percentageVolaFactor) / 100) *
    vdaFactor *
    nn2o_n2oFactor;

  this.consumptions.agriculturalActivityOrganicFertilizerConsumptions.forEach(
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    (values, _key) => {
      if (values.managementCulture != 'Arroz') {
        managementCulture =
          this.consumptions.agriculturalActivityManagementCultureConsumptions.get(
            values.managementCulture,
          );

        if (
          [
            'Composto orgânico',
            'Adubo orgânico geral',
            'Esterco Bovino, equino, suino, ovino',
            'Esterco Avícola',
          ].includes(values.substanceName)
        ) {
          organicFertilizer +=
            values.quantity *
            (this.emissionFactors.agriculturalActivityFertilizingFactors.get(
              values.substanceName,
            ).nitrogenPercentage /
              100) *
            managementCulture.area;
        }

        if (values.managementCulture == 'Cana') {
          if (values.substanceName == 'Vinhaça') {
            organicFertilizer +=
              values.quantity *
              (this.emissionFactors.agriculturalActivityFertilizingFactors.get(
                'Vinhaça',
              ).nitrogenPercentage /
                100) *
              managementCulture.area *
              1000;
          }
          if (values.substanceName == 'Torta de filtro') {
            organicFertilizer +=
              values.quantity *
              (this.emissionFactors.agriculturalActivityFertilizingFactors.get(
                'Torta de filtro',
              ).nitrogenPercentage /
                100) *
              managementCulture.area;
          }
        }
      }
    },
  );

  organicFertilizer =
    organicFertilizer *
    (percentageResidueFactor / 100) *
    vdaFactor *
    nn2o_n2oFactor;

  tonN2o = (organicFertilizer + syntheticFertilizer) / 1000;
  tonCo2Eq +=
    tonN2o *
    this.emissionFactors.globalWarmingPotentialGasFactors.get(
      'Óxido nitroso (N2O)',
    );

  return {
    tonCo2: tonCo2,
    tonCh4: tonCh4,
    tonN2o: tonN2o,
    tonCo2Eq: tonCo2Eq,
    tonCo2Bio: tonCo2Bio,
  };
}
