import { GhgProtocol } from './ghgProtocol';

export function setPowerConsumption(
  this: GhgProtocol,
  description: string,
  quantity: number,
) {
  return this.consumptions.powerConsumption.set(description, quantity);
}

export function calcPowerEmissions(this: GhgProtocol) {
  let tonCo2Eq = 0;
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  this.consumptions.powerConsumption.forEach((quantity, _key) => {
    tonCo2Eq +=
      quantity * this.emissionFactors.finFactorsByYear.get(this.inventaryYear);
  });
  return {
    tonCo2Eq: tonCo2Eq,
  };
}
