import {GhgProtocol} from '@amachains/ghg-protocol-br'

const ghgp = new GhgProtocol('org1', 'rua tal', 'nome da pessoa', 'Residencial, Agricultura, Florestal ou Pesca')
ghgp.setPowerConsumption('energia eletrica anual', 1000)

// Verificação
const respCalc = ghgp.calcPowerEmissions()

console.log(respCalc)
