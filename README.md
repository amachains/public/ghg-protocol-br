# GHG Protocol Br

Bliblioteca desenvolvida para facilitar a adoção das ferramentas do GHG em diferentes soluções.

- Calculadora Geral: ferramenta_ghg_protocol_v2023.0.3.xlsx
- Calculadora para Agricultura: GHG Protocol - Agricultura e pecuária v3.14.xls

## Getting started

1 - Veja nosso pacote no NPMJS

https://www.npmjs.com/package/@amachains/ghg-protocol-br

2 - Instale o pacote

```sh
yarn add  @amachains/ghg-protocol-br
```

3 - Importe no código fonte da aplicação

```js
import {GhgProtocol} from '@amachains/ghg-protocol-br'
```

4 - Inicialize o objeto GHG e use as funções de "set" e "calc" seguindo os arquivos de testes para aprender a usar a biblioteca
```js

const ghgp = new GhgProtocol('org1', 'rua tal', 'nome da pessoa', 'Residencial, Agricultura, Florestal ou Pesca')
ghgp.setPowerConsumption('energia eletrica anual', 1000)

const respCalc = ghgp.calcPowerEmissions()

console.log(respCalc)


```

https://gitlab.com/amachains/public/ghg-protocol-br/-/tree/main/test