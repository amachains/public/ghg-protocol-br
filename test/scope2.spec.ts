/* eslint-disable no-undef */

import { GhgProtocol } from '../src/ghgProtocol'
import { expect } from 'chai'
// const sinon = require('sinon')

describe('Testes de scope 2', () => {
  describe('Calculate Power Emission by FIN', () => {
    it('Deve retornar o valor de carbono de  4,2595548533 para 100 MWh', async () => {
      // Preparação
      const ghgp = new GhgProtocol('org1', 'rua tal', 'nome da pessoa', 'Residencial, Agricultura, Florestal ou Pesca')
      ghgp.setPowerConsumption('energia eletrica', 100)

      // Verificação
      const respCalc = ghgp.calcPowerEmissions()
      expect(respCalc.tonCo2Eq.toFixed(5)).to.equal((4.2595548533).toFixed(5))

      // cleanup
    })
  })
})