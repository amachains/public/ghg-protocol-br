/* eslint-disable no-undef */

import { GhgProtocol } from '../src/ghgProtocol'
import { expect } from 'chai'
// const sinon = require('sinon')

describe('Testes de scope 3', () => {
  describe('Calculate Cat 1 Emission', () => {
    it('Deve retornar o valor de carbono de  285 para 10 toneladas de CH4', async () => {
      // Preparação
      const ghgp = new GhgProtocol('org1', 'rua tal', 'nome da pessoa', 'Residencial, Agricultura, Florestal ou Pesca')
      ghgp.setCat1Consumption('produto comprado 1',{gasName: 'Metano (CH4)', quantity: 10})

      // Verificação
      const respCalc = ghgp.calcCat1Emissions()
      expect(respCalc.tonCo2Eq.toFixed(5)).to.equal((285).toFixed(5))

      // cleanup
    })
  })

  describe('Calculate Cat 2 Emission', () => {
    it('Deve retornar o valor de carbono de  285 para 10 toneladas de CH4', async () => {
      // Preparação
      const ghgp = new GhgProtocol('org1', 'rua tal', 'nome da pessoa', 'Residencial, Agricultura, Florestal ou Pesca')
      ghgp.setCat2Consumption('produto comprado 1',{gasName: 'Metano (CH4)', quantity: 10})

      // Verificação
      const respCalc = ghgp.calcCat2Emissions()
      expect(respCalc.tonCo2Eq.toFixed(5)).to.equal((285).toFixed(5))

      // cleanup
    })
  })
})