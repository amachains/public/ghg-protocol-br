/* eslint-disable no-undef */

import { GhgProtocol } from '../../src/ghgProtocol'
import { expect } from 'chai'
// const sinon = require('sinon')

describe('ghg Protocol pt-br Scope 1', () => {
  describe('Calculate Fugitive Gas Emissions', () => {
    // TODO: Ajustar a planilha. Penso que na planilha esta errada a formula, pois na documentação (incluindo nos texto na planilha) o capacityVariation deve ser subtraido, mas nas formulas da planilha esta somando.

    it.skip('Deve retornar um o valor de 2,80 de carbono para SV=30, TQ=60 e CC=10 kg de Metano (CH4)', async () => {
      // Preparação
      const ghgp = new GhgProtocol('org1', 'rua tal', 'nome da pessoa', 'Residencial, Agricultura, Florestal ou Pesca')
      ghgp.setFugitiveGasConsumption('Fonte de emissao 1', { gasName: 'Metano (CH4)', stockVariation: 30, transferedQuantity: 60, capacityVariation: 10 })
      // Verificação
      const respCalc = ghgp.calcFugitiveGasEmissions()
      expect(respCalc.tonCo2Eq.toFixed(2)).to.equal((2.80).toFixed(2))

      // cleanup
    })
  })

  describe('Calculate Fugitive SF6 e NF3 Gas Emissions', () => {
    it('Deve retornar um o valor de 1651,50 de carbono', async () => {
      // Preparação
      const ghgp = new GhgProtocol('org1', 'rua tal', 'nome da pessoa', 'Residencial, Agricultura, Florestal ou Pesca')
      ghgp.setFugitiveSF6andNF3Consumption('Fonte de emissao 1', { gasName: 'Hexafluoreto de enxofre (SF6)', stockInitialQuantity: 50, stockFinalQuantity: 5, purchasedQuantity: 15 })
      ghgp.setFugitiveSF6andNF3Consumption('Fonte de emissao 2', { gasName: 'Trifluoreto de nitrogênio (NF3)', stockInitialQuantity: 30, stockFinalQuantity: 25, purchasedQuantity: 10 })
      // Verificação
      const respCalc = ghgp.calcSf6andNf3FugitiveGasEmissions()
      expect(respCalc.tonCo2Eq.toFixed(2)).to.equal((1651.50).toFixed(2))

      // cleanup
    })
  })
  describe('Calculate Fugitive Other Source tools Gas Emissions', () => {
    it('Deve retornar um o valor de 2,6 de carbono', async () => {
      // Preparação
      const ghgp = new GhgProtocol('org1', 'rua tal', 'nome da pessoa', 'Residencial, Agricultura, Florestal ou Pesca')
      ghgp.setFugitiveOtherSourceToolsGasConsumption('Fonte de emissao 1', { gasName: 'HFC-41', quantity: 20 })
      ghgp.setFugitiveOtherSourceToolsGasConsumption('Fonte de emissao 2', { gasName: 'Metano (CH4)', quantity: 10 })
      // Verificação
      const respCalc = ghgp.calcFugitiveOtherSourceToolsGasEmissions()
      expect(respCalc.tonCo2Eq.toFixed(2)).to.equal((2.6).toFixed(2))

      // cleanup
    })

    it('Deve retornar um o valor de 2,6 de carbono usando função generica', async () => {
      // Preparação
      const ghgp = new GhgProtocol('org1', 'rua tal', 'nome da pessoa', 'Residencial, Agricultura, Florestal ou Pesca')
      ghgp.setFugitiveOtherSourceToolsGasConsumption('Fonte de emissao 1', { gasName: 'HFC-41', quantity: 20 })
      ghgp.setFugitiveOtherSourceToolsGasConsumption('Fonte de emissao 2', { gasName: 'Metano (CH4)', quantity: 10 })
      // Verificação
      const respCalc = ghgp.calcGenericEmissions(ghgp.consumptions.fugitiveOtherSourceToolsGasConsumptions, ghgp.emissionFactors.globalWarmingPotentialGasFactors)
      expect(respCalc.tonCo2Eq.toFixed(2)).to.equal((2.6).toFixed(2))

      // cleanup
    })
  })
  
})
