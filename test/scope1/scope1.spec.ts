/* eslint-disable no-undef */

import { GhgProtocol } from '../../src/ghgProtocol'
import { expect } from 'chai'
// const sinon = require('sinon')

describe('ghg Protocol pt-br Scope 1', () => {
  describe('Calculate Industrial Process Emissions', () => {
    it('Deve retornar um o valor de 2,6 de carbono', async () => {
      // Preparação
      const ghgp = new GhgProtocol('org1', 'rua tal', 'nome da pessoa', 'Residencial, Agricultura, Florestal ou Pesca')
      ghgp.setIndustrialProcessGasConsumption('Fonte de emissao 1', { gasName: 'HFC-41', quantity: 20 })
      ghgp.setIndustrialProcessGasConsumption('Fonte de emissao 2', { gasName: 'Metano (CH4)', quantity: 10 })
      // Verificação
      const respCalc = ghgp.calcIndustrialProcessEmissions()
      expect(respCalc.tonCo2Eq.toFixed(2)).to.equal((2.6).toFixed(2))

      // cleanup
    })
  })

  describe('Calculate Agricultural Activity Emissions', () => {
    it('Deve retornar um o valor de 2,6 de carbono', async () => {
      // Preparação
      const ghgp = new GhgProtocol('org1', 'rua tal', 'nome da pessoa', 'Residencial, Agricultura, Florestal ou Pesca')
      ghgp.setAgriculturalActivityGasConsumption('Fonte de emissao 1', { gasName: 'HFC-41', quantity: 20 })
      ghgp.setAgriculturalActivityGasConsumption('Fonte de emissao 2', { gasName: 'Metano (CH4)', quantity: 10 })
      // Verificação
      const respCalc = ghgp.calcAgriculturalActivityEmissions()
      expect(respCalc.tonCo2Eq.toFixed(2)).to.equal((2.6).toFixed(2))

      // cleanup
    })
  })

  describe('Calculate Soil Change Emissions', () => {
    it('Deve retornar um o valor de 3300 de carbono', async () => {
      // Preparação
      const ghgp = new GhgProtocol('org1', 'rua tal', 'nome da pessoa', 'Residencial, Agricultura, Florestal ou Pesca')
      ghgp.setSoilChangeGasConsumption('Fonte de emissao 1', { gasName: 'Óxido nitroso (N2O)', quantity: 10 })
      ghgp.setSoilChangeGasConsumption('Fonte de emissao 2', { gasName: 'Metano (CH4)', quantity: 20 })
      // Verificação
      const respCalc = ghgp.calcSoilChangeActivityEmissions()
      expect(respCalc.tonCo2Eq.toFixed(2)).to.equal((3300).toFixed(2))

      // cleanup
    })
  })
})
