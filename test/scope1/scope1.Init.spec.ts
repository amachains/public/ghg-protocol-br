/* eslint-disable no-undef */

import { GhgProtocol } from '../../src/ghgProtocol'
import { expect } from 'chai'
// const sinon = require('sinon')

describe('ghg Protocol pt-br Scope 1', () => {
  describe('Testes de inicialização', () => {
    it('Deve iniciar chamar o construtor da class e retornar um objeto', async () => {
      // Preparação
      const ghgp = new GhgProtocol('org1', 'rua tal', 'nome da pessoa', 'Residencial, Agricultura, Florestal ou Pesca')
      // Verificação

      expect(ghgp).to.be.an('object')
      // cleanup
    })

    it('Testar o setStationatyFuelConsumption com um combustivel desconhecido', async () => {
      // Preparação
      const ghgp = new GhgProtocol('org1', 'rua tal', 'nome da pessoa', 'Residencial, Agricultura, Florestal ou Pesca')
      ghgp.setStationaryFuelConsumption('forno industrial', { fuelName: 'gasolina desconhecida', quantity: 10 })
      // Verificação
      expect(ghgp.consumptions.stationaryFuelConsumptions.size).to.equal(0)

      // cleanup
    })

    it('Testar o setStationatyFuelConsumption com um combustivel valido (Acetileno)', async () => {
      // Preparação
      const ghgp = new GhgProtocol('org1', 'rua tal', 'nome da pessoa', 'Residencial, Agricultura, Florestal ou Pesca')
      ghgp.setStationaryFuelConsumption('forno industrial', { fuelName: 'Acetileno', quantity: 10 })

      // Verificação
      expect(ghgp.consumptions.stationaryFuelConsumptions.size).to.be.equal(1)

      // cleanup
    })
  })
})
