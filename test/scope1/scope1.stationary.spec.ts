/* eslint-disable no-undef */

import { GhgProtocol } from '../../src/ghgProtocol'
import { expect } from 'chai'
// const sinon = require('sinon')

describe('ghg Protocol pt-br Scope 1', () => {
  describe('Calculate Stationary Emissions', () => {
    it('Deve retornar o valor de 0.16188 de carbono para 1000 litros de Gasolina Automotiva (comercial)', async () => {
      // Preparação
      const ghgp = new GhgProtocol('org1', 'rua tal', 'nome da pessoa', 'Residencial, Agricultura, Florestal ou Pesca')
      ghgp.setStationaryFuelConsumption('Fonte de emissao', { fuelName: 'Gasolina Automotiva (comercial)', quantity: 1000 })
      // Verificação
      const respCalc = ghgp.calcStationaryEmissions()
      expect(respCalc.tonCo2Eq.toFixed(2)).to.equal((1.647468).toFixed(2))

      // cleanup
    })

    it('Deve retornar um o valor de 0.072 de carbono para 5000 litros de  Biodiesel (B100) ', async () => {
      // Preparação
      const ghgp = new GhgProtocol('org1', 'rua tal', 'nome da pessoa', 'Residencial, Agricultura, Florestal ou Pesca')
      ghgp.setStationaryFuelConsumption('Fonte de emissao meu motor', { fuelName: 'Biodiesel (B100)', quantity: 5000 })
      // Verificação
      const respCalc = ghgp.calcStationaryEmissions()
      expect(respCalc.tonCo2Eq.toFixed(2)).to.equal((0.072785).toFixed(2))

      // cleanup
    })

    it('Deve retornar um o valor de 11,916 de carbono para 5000 litros de Óleo Diesel (comercial)', async () => {
    // Preparação
      const ghgp = new GhgProtocol('org1', 'rua tal', 'nome da pessoa', 'Residencial, Agricultura, Florestal ou Pesca')
      ghgp.setStationaryFuelConsumption('Fonte de emissao meu motor', { fuelName: 'Óleo Diesel (comercial)', quantity: 5000 })
      // Verificação
      const respCalc = ghgp.calcStationaryEmissions()
      expect(respCalc.tonCo2Eq.toFixed(2)).to.equal((11.916).toFixed(2))

    // cleanup
    })

    it('Deve retornar um o valor de 320.84 de carbono para 100 toneladas de  Alcatrão ', async () => {
    // Preparação
      const ghgp = new GhgProtocol('org1', 'rua tal', 'nome da pessoa', 'Residencial, Agricultura, Florestal ou Pesca')
      ghgp.setStationaryFuelConsumption('Fonte de emissao', { fuelName: 'Alcatrão', quantity: 100 })
      // Verificação
      const respCalc = ghgp.calcStationaryEmissions()
      expect(respCalc.tonCo2Eq.toFixed(2)).to.equal((320.84).toFixed(2))

    // cleanup
    })

    it('Deve retornar o valor de 314.06 como somátorio do carbono para 100 toneladas de  Querosene de Aviação e 100 toneladas de  Biometano', async () => {
      // Preparação
      const ghgp = new GhgProtocol('org1', 'rua tal', 'nome da pessoa', 'Residencial, Agricultura, Florestal ou Pesca')
      ghgp.setStationaryFuelConsumption('Primeira fonte de emissao', { fuelName: 'Querosene de Aviação', quantity: 100 })
      ghgp.setStationaryFuelConsumption('Segunda fonte de emissao', { fuelName: 'Biometano', quantity: 100 })
      // Verificação
      const respCalc = ghgp.calcStationaryEmissions()
      expect(respCalc.tonCo2Eq.toFixed(2)).to.equal((314.12).toFixed(2))

      // cleanup
    })
  })

 
})
