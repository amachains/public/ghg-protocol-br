/* eslint-disable no-undef */

import { GhgProtocol } from '../../src/ghgProtocol'
import { expect } from 'chai'
// const sinon = require('sinon')

describe('ghg Protocol pt-br Scope 1', () => {
  describe('Calculate Mobile Road Emissions', () => {
    it('Deve retornar um o valor de 1.69 de carbono para 1000 litros de Gasolina Automotiva (comercial)', async () => {
      // Preparação
      const ghgp = new GhgProtocol('org1', 'rua tal', 'nome da pessoa', 'Residencial, Agricultura, Florestal ou Pesca')
      ghgp.setMobileRoadFuelConsumption('Fonte de emissao', { fuelName: 'Gasolina Automotiva (comercial)', quantity: 1000 })
      // Verificação
      const respCalc = ghgp.calcMobileRoadEmissions()
      expect(respCalc.tonCo2Eq.toFixed(2)).to.equal((1.69).toFixed(2))

      // cleanup
    })

    it('Deve retornar um o valor de 0.072 de carbono para 5000 litros de  Biodiesel (B100) ', async () => {
      // Preparação
      const ghgp = new GhgProtocol('org1', 'rua tal', 'nome da pessoa', 'Residencial, Agricultura, Florestal ou Pesca')
      ghgp.setMobileRoadFuelConsumption('Fonte de emissao meu motor', { fuelName: 'Biodiesel (B100)', quantity: 5000 })
      // Verificação
      const respCalc = ghgp.calcMobileRoadEmissions()
      expect(respCalc.tonCo2Eq.toFixed(2)).to.equal((0.072785).toFixed(2))

      // cleanup
    })

    it('Deve retornar um o valor de 11,91 de carbono para 5000 litros de Óleo Diesel (comercial)', async () => {
    // Preparação
      const ghgp = new GhgProtocol('org1', 'rua tal', 'nome da pessoa', 'Residencial, Agricultura, Florestal ou Pesca')
      ghgp.setMobileRoadFuelConsumption('Fonte de emissao meu motor', { fuelName: 'Óleo Diesel (comercial)', quantity: 5000 })
      // Verificação
      const respCalc = ghgp.calcMobileRoadEmissions()
      expect(respCalc.tonCo2Eq.toFixed(2)).to.equal((11.91).toFixed(2))

    // cleanup
    })

    it('Deve retornar um o valor de 0,2123200585 de carbono para 100 toneladas de Gás Natural Veicular (GNV)', async () => {
    // Preparação
      const ghgp = new GhgProtocol('org1', 'rua tal', 'nome da pessoa', 'Residencial, Agricultura, Florestal ou Pesca')
      ghgp.setMobileRoadFuelConsumption('Fonte de emissao', { fuelName: 'Gás Natural Veicular (GNV)', quantity: 100 })
      // Verificação
      const respCalc = ghgp.calcMobileRoadEmissions()
      expect(respCalc.tonCo2Eq.toFixed(2)).to.equal((0.2123200585).toFixed(2))

    // cleanup
    })

    it('Deve retornar o valor de 0,3029765310 como somátorio do carbono para 100 toneladas de  Querosene de Aviação e 100 toneladas de  Biometano', async () => {
      // Preparação
      const ghgp = new GhgProtocol('org1', 'rua tal', 'nome da pessoa', 'Residencial, Agricultura, Florestal ou Pesca')
      ghgp.setMobileRoadFuelConsumption('Primeira fonte de emissao', { fuelName: 'Gás Liquefeito de Petróleo (GLP)', quantity: 100 })
      ghgp.setMobileRoadFuelConsumption('Segunda fonte de emissao', { fuelName: 'Etanol Hidratado', quantity: 100 })
      // Verificação
      const respCalc = ghgp.calcMobileRoadEmissions()
      expect(respCalc.tonCo2Eq.toFixed(2)).to.equal((0.3029765310).toFixed(2))

      // cleanup
    })
  })

  describe('Calculate Mobile Rail Emissions', () => {
    it('Deve retornar um o valor de 1.69 de carbono para 1000 litros de Gasolina Automotiva (comercial)', async () => {
      // Preparação
      const ghgp = new GhgProtocol('org1', 'rua tal', 'nome da pessoa', 'Residencial, Agricultura, Florestal ou Pesca')
      ghgp.setMobileRailFuelConsumption('Fonte de emissao', { fuelName: 'Gasolina Automotiva (comercial)', quantity: 1000 })
      // Verificação
      const respCalc = ghgp.calcMobileRailEmissions()
      expect(respCalc.tonCo2Eq.toFixed(2)).to.equal((1.69).toFixed(2))

      // cleanup
    })

    it('Deve retornar um o valor de 0.072 de carbono para 5000 litros de  Biodiesel (B100) ', async () => {
      // Preparação
      const ghgp = new GhgProtocol('org1', 'rua tal', 'nome da pessoa', 'Residencial, Agricultura, Florestal ou Pesca')
      ghgp.setMobileRailFuelConsumption('Fonte de emissao meu motor', { fuelName: 'Biodiesel (B100)', quantity: 5000 })
      // Verificação
      const respCalc = ghgp.calcMobileRailEmissions()
      expect(respCalc.tonCo2Eq.toFixed(2)).to.equal((0.072785).toFixed(2))

      // cleanup
    })

    it('Deve retornar um o valor de 11,91 de carbono para 5000 litros de Óleo Diesel (comercial)', async () => {
    // Preparação
      const ghgp = new GhgProtocol('org1', 'rua tal', 'nome da pessoa', 'Residencial, Agricultura, Florestal ou Pesca')
      ghgp.setMobileRailFuelConsumption('Fonte de emissao meu motor', { fuelName: 'Óleo Diesel (comercial)', quantity: 5000 })
      // Verificação
      const respCalc = ghgp.calcMobileRailEmissions()
      expect(respCalc.tonCo2Eq.toFixed(2)).to.equal((11.91).toFixed(2))

    // cleanup
    })

    it('Deve retornar um o valor de 0,2123200585 de carbono para 100 toneladas de Gás Natural Veicular (GNV)', async () => {
    // Preparação
      const ghgp = new GhgProtocol('org1', 'rua tal', 'nome da pessoa', 'Residencial, Agricultura, Florestal ou Pesca')
      ghgp.setMobileRailFuelConsumption('Fonte de emissao', { fuelName: 'Gás Natural Veicular (GNV)', quantity: 100 })
      // Verificação
      const respCalc = ghgp.calcMobileRailEmissions()
      expect(respCalc.tonCo2Eq.toFixed(2)).to.equal((0.2123200585).toFixed(2))

    // cleanup
    })

    it('Deve retornar o valor de 0,3029765310 como somátorio do carbono para 100 toneladas de  Querosene de Aviação e 100 toneladas de  Biometano', async () => {
      // Preparação
      const ghgp = new GhgProtocol('org1', 'rua tal', 'nome da pessoa', 'Residencial, Agricultura, Florestal ou Pesca')
      ghgp.setMobileRailFuelConsumption('Primeira fonte de emissao', { fuelName: 'Gás Liquefeito de Petróleo (GLP)', quantity: 100 })
      ghgp.setMobileRailFuelConsumption('Segunda fonte de emissao', { fuelName: 'Etanol Hidratado', quantity: 100 })
      // Verificação
      const respCalc = ghgp.calcMobileRailEmissions()
      expect(respCalc.tonCo2Eq.toFixed(2)).to.equal((0.3029765310).toFixed(2))

      // cleanup
    })

    it('Deve retornar um o valor de 159,59 de carbono para 100 toneladas de Carvão Vapor 4200 kcal / kg', async () => {
      // Preparação
      const ghgp = new GhgProtocol('org1', 'rua tal', 'nome da pessoa', 'Residencial, Agricultura, Florestal ou Pesca')
      ghgp.setMobileRailFuelConsumption('Fonte de emissao', { fuelName: 'Carvão Vapor 4200 kcal / kg', quantity: 100 })
      // Verificação
      const respCalc = ghgp.calcMobileRailEmissions()
      expect(respCalc.tonCo2Eq.toFixed(2)).to.equal((159.59).toFixed(2))

      // cleanup
    })
  })

  describe('Calculate Mobile Hydro Emissions', () => {
    it('Deve retornar um o valor de 1.69 de carbono para 1000 litros de Gasolina Automotiva (comercial)', async () => {
      // Preparação
      const ghgp = new GhgProtocol('org1', 'rua tal', 'nome da pessoa', 'Residencial, Agricultura, Florestal ou Pesca')
      ghgp.setMobileHydroFuelConsumption('Fonte de emissao', { fuelName: 'Gasolina Automotiva (comercial)', quantity: 1000 })
      // Verificação
      const respCalc = ghgp.calcMobileHydroEmissions()
      expect(respCalc.tonCo2Eq.toFixed(2)).to.equal((1.69).toFixed(2))

      // cleanup
    })

    it('Deve retornar um o valor de 0.072 de carbono para 5000 litros de  Biodiesel (B100) ', async () => {
      // Preparação
      const ghgp = new GhgProtocol('org1', 'rua tal', 'nome da pessoa', 'Residencial, Agricultura, Florestal ou Pesca')
      ghgp.setMobileHydroFuelConsumption('Fonte de emissao meu motor', { fuelName: 'Biodiesel (B100)', quantity: 5000 })
      // Verificação
      const respCalc = ghgp.calcMobileHydroEmissions()
      expect(respCalc.tonCo2Eq.toFixed(2)).to.equal((0.072785).toFixed(2))

      // cleanup
    })

    it('Deve retornar um o valor de 11,91 de carbono para 5000 litros de Óleo Diesel (comercial)', async () => {
    // Preparação
      const ghgp = new GhgProtocol('org1', 'rua tal', 'nome da pessoa', 'Residencial, Agricultura, Florestal ou Pesca')
      ghgp.setMobileHydroFuelConsumption('Fonte de emissao meu motor', { fuelName: 'Óleo Diesel (comercial)', quantity: 5000 })
      // Verificação
      const respCalc = ghgp.calcMobileHydroEmissions()
      expect(respCalc.tonCo2Eq.toFixed(2)).to.equal((11.91).toFixed(2))

    // cleanup
    })

    it('Deve retornar um o valor de 0,2123200585 de carbono para 100 toneladas de Gás Natural Veicular (GNV)', async () => {
    // Preparação
      const ghgp = new GhgProtocol('org1', 'rua tal', 'nome da pessoa', 'Residencial, Agricultura, Florestal ou Pesca')
      ghgp.setMobileHydroFuelConsumption('Fonte de emissao', { fuelName: 'Gás Natural Veicular (GNV)', quantity: 100 })
      // Verificação
      const respCalc = ghgp.calcMobileHydroEmissions()
      expect(respCalc.tonCo2Eq.toFixed(2)).to.equal((0.2123200585).toFixed(2))

    // cleanup
    })

    it('Deve retornar o valor de 0,3029765310 como somátorio do carbono para 100 toneladas de  Querosene de Aviação e 100 toneladas de  Biometano', async () => {
      // Preparação
      const ghgp = new GhgProtocol('org1', 'rua tal', 'nome da pessoa', 'Residencial, Agricultura, Florestal ou Pesca')
      ghgp.setMobileHydroFuelConsumption('Primeira fonte de emissao', { fuelName: 'Gás Liquefeito de Petróleo (GLP)', quantity: 100 })
      ghgp.setMobileHydroFuelConsumption('Segunda fonte de emissao', { fuelName: 'Etanol Hidratado', quantity: 100 })
      // Verificação
      const respCalc = ghgp.calcMobileHydroEmissions()
      expect(respCalc.tonCo2Eq.toFixed(2)).to.equal((0.3029765310).toFixed(2))

      // cleanup
    })
  })

  describe('Calculate Mobile Air Emissions', () => {
    it('Deve retornar um o valor de 2,5360183296 de carbono para 1000 litros de Querosene de Aviação', async () => {
      // Preparação
      const ghgp = new GhgProtocol('org1', 'rua tal', 'nome da pessoa', 'Residencial, Agricultura, Florestal ou Pesca')
      ghgp.setMobileAirFuelConsumption('Fonte de emissao', { fuelName: 'Querosene de Aviação', quantity: 1000 })
      // Verificação
      const respCalc = ghgp.calcMobileAirEmissions()
      expect(respCalc.tonCo2Eq.toFixed(2)).to.equal((2.5360183296).toFixed(2))

      // cleanup
    })

    it('Deve retornar um o valor de 11,3404800960 de carbono para 5000 litros de Gasolina de Aviação', async () => {
      // Preparação
      const ghgp = new GhgProtocol('org1', 'rua tal', 'nome da pessoa', 'Residencial, Agricultura, Florestal ou Pesca')
      ghgp.setMobileAirFuelConsumption('Fonte de emissao meu motor', { fuelName: 'Gasolina de Aviação', quantity: 5000 })
      // Verificação
      const respCalc = ghgp.calcMobileAirEmissions()
      expect(respCalc.tonCo2Eq.toFixed(2)).to.equal((11.3404800960).toFixed(2))

      // cleanup
    })
  })  
})
